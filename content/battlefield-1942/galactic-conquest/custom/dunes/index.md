---
title: Dunes
author: Jeff (Zang)
description: ''
date: '2004-02-17'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
  - TDM
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: custom
  controlpoints:
    - name: Imperial Base
      id: ImperialBase
      position:
        x: '241.81'
        'y': '73.24'
        z: '278.04'
    - name: Rebel Base
      id: RebelBase
      position:
        x: '1775.51'
        'y': '80.28'
        z: '1618.98'
    - name: New Control Point 1
      id: New_Control_Point1
      position:
        x: '394.59'
        'y': '66.22'
        z: '755.30'
    - name: New Control Point 2
      id: New_Control_Point2
      position:
        x: '1197.68'
        'y': '80.76'
        z: '910.14'
    - name: New Control Point 3
      id: New_Control_Point3
      position:
        x: '198.10'
        'y': '58.18'
        z: '1754.03'
    - name: New Control Point 4
      id: New_Control_Point4
      position:
        x: '793.92'
        'y': '65.30'
        z: '1321.72'
  created: '2004-02-17'
  creator: Jeff (Zang)
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-dunes.html
---

