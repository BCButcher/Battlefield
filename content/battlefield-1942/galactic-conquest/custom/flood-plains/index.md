---
title: Flood Plains
author: Tortel
description: ''
date: '2004-09-25'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: custom
  controlpoints:
    - name: Flooded Village
      id: Flooded_Village
      position:
        x: '181.29'
        'y': '76.06'
        z: '1669.87'
    - name: Sunken Ship
      id: Sunken_Ship
      position:
        x: '349.16'
        'y': '79.60'
        z: '611.02'
    - name: Old Reaserch Center
      id: Old_Reaserch_Center
      position:
        x: '1949.48'
        'y': '86.74'
        z: '1833.27'
  created: '2004-09-25'
  creator: Tortel
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-flood-plains.html
---

