---
title: Galactic Conquest Custom Maps
name: Custom
description: >-
  Galactic Conquest Extended (GCX) is a mini-mod that adds bot support to
  Galactic Conquest and extends the GCmod by doing total conversions of existing
  maps.
categories:
  - Battlefield 1942
  - Galactic Conquest
  - Galactic Conquest Extended
type: source
layout: layouts/source.njk
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/
---

