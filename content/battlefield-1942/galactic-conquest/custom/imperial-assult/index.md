---
title: Imperial Assult
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: custom
  controlpoints:
    - name: Tantive III
      id: Tantive_III
      position:
        x: '235.70'
        'y': '86.42'
        z: '786.63'
    - name: Cantina
      id: Cantina
      position:
        x: '1318.17'
        'y': '80.60'
        z: '1349.71'
    - name: City
      id: City
      position:
        x: '1743.10'
        'y': '93.39'
        z: '847.96'
    - name: Supply Depot
      id: Supply_Depot
      position:
        x: '739.16'
        'y': '78.81'
        z: '1070.70'
    - name: Rebel Camp
      id: Rebel_Camp
      position:
        x: '677.62'
        'y': '80.89'
        z: '343.53'
---

