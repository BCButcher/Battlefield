---
title: Imperial Air Training
author: ''
description: >-
  Welcome to Imperial Air Training, Its Alpha squad versus Beta Squad, Live
  Fire, and Live Kill, only the best serve Emporer Palpatine, 2 replica
  Millenium Falcons have been provided as extra targets.
date: '2004-09-29'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: custom
  controlpoints:
    - name: Controlpoint Allies Base
      id: controlpoint_allies_base
      position:
        x: '1318.97'
        'y': '63.1867'
        z: '672.199'
    - name: Controlpoint Axis Base
      id: controlpoint_axis_base
      position:
        x: '1365.13'
        'y': '63.2788'
        z: '1183.62'
  created: '2004-09-29'
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-imperial-air-training-map.html
---

