---
title: Dogfighting
author: ''
description: >-
  A squad of Tie Fighters faces of against a fleet of Rebel Awings for Air
  Superiority of this remote section of Icy waste
date: '2004-02-03'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: custom
  controlpoints:
    - name: Controlpoint Allies Base
      id: controlpoint_allies_base
      position:
        x: '1318.97'
        'y': '63.1867'
        z: '672.199'
    - name: Controlpoint Axis Base
      id: controlpoint_axis_base
      position:
        x: '1365.13'
        'y': '63.2788'
        z: '1183.62'
  created: '2004-02-03'
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-dogfighting.html
---

