---
title: Selonia
author: Lowie_Smurf
description: >-
  2 Years after the destruction of the second deathstar the new republic is
  growing. In it's way to free corusant that is still in the hands of the
  empire. for that the republic needs to have a good base. For that the planet
  Selonia was chosen. Unfortunately the empirals descovered this and attacked
  the rebelfleet in space. After a big fight the have landed on the planet and
  continue fighting. It is up to you to battle.May the force be with you!
date: '2004-05-15'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: custom
  controlpoints:
    - name: Rebel Base
      id: Rebel_base
      position:
        x: '1719.94'
        'y': '125.84'
        z: '206.98'
    - name: Rebel Coast
      id: Rebel_coast
      position:
        x: '1245.46'
        'y': '125.90'
        z: '459.04'
    - name: Island
      id: Island
      position:
        x: '453.36'
        'y': '126.00'
        z: '113.63'
    - name: Imperial Base
      id: Imperial_base
      position:
        x: '520.26'
        'y': '127.00'
        z: '1520.21'
    - name: Imperial Coast
      id: Imperial_coast
      position:
        x: '813.84'
        'y': '126.00'
        z: '1518.28'
    - name: Island 2
      id: Island2
      position:
        x: '1582.93'
        'y': '125.89'
        z: '1198.40'
    - name: Island 3
      id: Island3
      position:
        x: '501.23'
        'y': '125.94'
        z: '1070.02'
  created: '2004-05-15'
  creator: Lowie_Smurf
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-selonia.html
---

