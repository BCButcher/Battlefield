---
title: Return To Hoth
author: ''
description: >-
  After the destruction of the Second Death Star, Rebel forces dispatch an
  invasion force to whipe out the garrison left behind on Hoth and free rebel
  prisoners of War. With them they have brought Snowspeeders modified for heavy
  ground assualt while the empire attempts to rebuild its mighty walkers
date: '2007-02-10'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: custom
  controlpoints: []
  created: '2007-02-10'
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-return-to-hoth.html
---

