---
title: Swamps Of Dagobah
author: Django
description: Prepare for an infantry-only battle in the swamps of Dagobah.
date: '2015-09-13'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
  - Coop
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: redux
  controlpoints:
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '437.32'
        'y': '77.85'
        z: '238.39'
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: '568.06'
        'y': '76.64'
        z: '849.96'
    - name: Openbase Lumbermill Cpoint
      id: openbase_lumbermill_Cpoint
      position:
        x: '584.33'
        'y': '73.64'
        z: '426.79'
    - name: Openbasecammo
      id: openbasecammo
      position:
        x: '508.83'
        'y': '75.99'
        z: '582.20'
    - name: New Control Point 1
      id: New_Control_Point1
      position:
        x: '597.77'
        'y': '75.88'
        z: '640.73'
  created: '2015-09-13'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

