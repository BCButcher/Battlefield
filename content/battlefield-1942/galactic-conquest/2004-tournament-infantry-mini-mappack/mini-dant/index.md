---
title: Mini Dant
author: GC Team
description: ''
date: '2004-10-13'
categories:
  - Battlefield 1942
  - 2004 Tournament Infantry Mini Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: 2004-tournament-infantry-mini-mappack
  controlpoints:
    - name: Impmaintemplate
      id: impmaintemplate
      position:
        x: '1229.28'
        'y': '0.0883'
        z: '1012.49'
    - name: Imperial Sidetemplate
      id: imperial_sidetemplate
      position:
        x: '1103.75'
        'y': '62.1938'
        z: '1045.76'
    - name: The Bridgetemplate
      id: the_bridgetemplate
      position:
        x: '1054.9'
        'y': '60.9957'
        z: '920.268'
    - name: Rebel Sidetemplate
      id: rebel_sidetemplate
      position:
        x: '959.016'
        'y': '62.2242'
        z: '848.886'
    - name: Rebelmaintemplate
      id: rebelmaintemplate
      position:
        x: '864.201'
        'y': '0.7305'
        z: '783.239'
  created: '2004-10-13'
  creator: GC Team
---

