---
title: Galactic Conquest
name: Galactic Conquest
description: >-
  Battlefield: Galactic Conquest will put you in the role of an every day
  soldier in the Star Wars universe. You will be able to choose between the
  Galactic Civil war and The Clone Wars and fight by either means of Land or Air
  in many of the Vehicles you can see throughout the Star Wars universe. Take
  out AT AT walkers with your snowspeeder or battle around huge star destroyers
  in space, whatever takes your fancy!


  It is a period of civil war. The Galactic Empire faces a constant threat from
  Rebel forces, who wish to to bring freedom to all those oppressed by the
  Empire. You must choose whether your allegiance lies with the Empire, or with
  the underdog Rebels. Imperials: Get those Rebel scum! Rebels: May the Force be
  with you.
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
banner: true
---

