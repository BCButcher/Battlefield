---
title: Kessel
author: Django, GC Team
description: >-
  GC_Kessel, once a widespread exporter of spice through out the outer rim has
  been locked down by the Empire. The largest of the mining colonies has been
  transformed the into a Imperial Correction Facility. Only criminals with the
  most severe crimes of betraying the Empire are sentenced to GC_Kessel. They
  are forced to work the pitch black spice mines for the rest of their lives. In
  the chaos of the civil war a rebel fleet has flanked the planetoid defenses
  and deployed ground forces to take control of the colony and rescue the
  prisoners...
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: 2011-mappack
  controlpoints:
    - name: Rebrunner CP
      id: Rebrunner_CP
      position:
        x: '1491.15'
        'y': '120.21'
        z: '467.09'
    - name: Nebulon Base
      id: Nebulon_Base
      position:
        x: '1538.08'
        'y': '832.20'
        z: '288.98'
    - name: Spice Mine
      id: SpiceMine
      position:
        x: '1578.64'
        'y': '127.08'
        z: '1973.63'
    - name: Landing Platform 3 CP
      id: Landing_Platform3_CP
      position:
        x: '1453.58'
        'y': '165.46'
        z: '1550.01'
    - name: Landing Platform 2 CP
      id: Landing_Platform2_CP
      position:
        x: '479.41'
        'y': '147.18'
        z: '1741.03'
    - name: Landing Platform 1 CP
      id: Landing_Platform1_CP
      position:
        x: '512.11'
        'y': '165.48'
        z: '976.30'
  created: '2011-04-01'
  creator: Django, GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

