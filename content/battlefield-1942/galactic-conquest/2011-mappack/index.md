---
title: 2011 Map Pack
name: 2011 Mappack
description: >-
  This mappack was modded and put together by a small team between August 2010
  and March 2011. It contains 6 partially completely revised custom maps from
  the years 2005-2006 and 2 new maps from 2010/2011. In addition, there are
  numerous new animated textures and a new menu background.
categories:
  - Battlefield 1942
  - Galactic Conquest
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

