---
title: Alaris Prime
author: GC Team
description: >-
  Rebel intelligence has discovered the location of an ION Cannon at an Imperial
  Compound. It is believed the cannon is responsible for destroying multiple
  vessels near the Wookie planet of Alaris Prime. The Rebellion will not stand
  for such an agressive move by The Empire.
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Objectives
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Objectives
  size: 1024
  source: 2011-mappack
  controlpoints: []
  created: '2011-04-01'
  creator: GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

