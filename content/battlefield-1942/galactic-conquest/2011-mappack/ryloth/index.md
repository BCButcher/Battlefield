---
title: Ryloth
author: GC Team
description: >-
  The Months following the Empire's defeat at the Battle of Endor were far from
  peaceful. Loyal Imperial forces dug into their planetary bases, determined to
  lose no more ground to what they still considered to be Rebel insurgents. The
  fragile New Republic was forced to wage bloody campaigns on forsaken planets
  to rout them. On the baked world of Ryloth the Alliance forces prepare to take
  to arms once again, against a desperate Imperial stronghold. The Battle for
  Ryloth has begun...
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: 2011-mappack
  controlpoints:
    - name: Main Entrance
      id: Main_Entrance
      position:
        x: '827.61'
        'y': '94.70'
        z: '1533.49'
    - name: Rebel LZ
      id: Rebel_LZ
      position:
        x: '1358.82'
        'y': '93.69'
        z: '295.56'
    - name: Mountain Outpost
      id: Mountain_Outpost
      position:
        x: '657.43'
        'y': '103.62'
        z: '1233.41'
    - name: Forward Fire Base
      id: Forward_Fire_Base
      position:
        x: '772.10'
        'y': '72.50'
        z: '565.70'
    - name: Forward Trenches
      id: Forward_Trenches
      position:
        x: '873.26'
        'y': '69.26'
        z: '1015.18'
    - name: Main Base
      id: Main_Base
      position:
        x: '780.44'
        'y': '70.87'
        z: '1428.54'
    - name: Second Trench Line
      id: Second_trench_line
      position:
        x: '775.85'
        'y': '63.40'
        z: '1247.77'
  created: '2011-04-01'
  creator: GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

