---
title: Endoth
author: Dnamro
description: Battle for the planet Endoth. Map by Rolo Tomasi. GCX support by Dnamro.
date: '2004-11-26'
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: extended
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '824.51'
        'y': '56.1'
        z: '1081.7'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '675.3'
        'y': '73.93'
        z: '263.1'
    - name: Cammobunker
      id: cammobunker
      position:
        x: '983.121'
        'y': '54.9258'
        z: '764.564'
    - name: City
      id: city
      position:
        x: '860.903'
        'y': '59.871'
        z: '579.522'
    - name: Bridgebunker
      id: bridgebunker
      position:
        x: '649.656'
        'y': '59.611'
        z: '623.812'
    - name: Windmill
      id: windmill
      position:
        x: '753.9'
        'y': '82.2'
        z: '861.85'
    - name: The Village
      id: the_village
      position:
        x: '275.809'
        'y': '106'
        z: '331.988'
  created: '2004-11-26'
  creator: Dnamro
origin: https://www.gamefront.com/games/battlefield-1942/file/gcx-endoth
---

