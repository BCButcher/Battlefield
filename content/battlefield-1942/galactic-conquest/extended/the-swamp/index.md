---
title: The Swamp
author: Dnamro
description: Prepare for an infantry-only battle in the swamps of Dagobah.
date: '2004-03-21'
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 0
  source: extended
  controlpoints:
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '437.315002'
        'y': '77.854691'
        z: '238.389999'
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: '568.057983'
        'y': '76.640625'
        z: '849.955994'
    - name: Openbase Lumbermill Cpoint
      id: openbase_lumbermill_Cpoint
      position:
        x: '639.658020'
        'y': '76.492973'
        z: '556.038025'
    - name: Openbasecammo
      id: openbasecammo
      position:
        x: '508.833008'
        'y': '73.994537'
        z: '582.200012'
  created: '2004-03-21'
  creator: Dnamro
origin: >-
  https://web.archive.org/web/20240109160047/https://www.softpaz.com/games/download-battlefield-1942-galactic-conquest-extended-mod-game-13771.htm
---

