---
title: Endoria
author: Dnamro
description: >-
  Imperial forces are trying to take over Endoria,Endors sister planet.The rebel
  forces have landed at the northern end of the village and need to eliminate
  the imperial forces.As the imperial army you need to secure this outpost from
  the rebels.
date: '2003-08-13'
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: extended
  controlpoints:
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: '1799.46'
        'y': '38.70'
        z: '1960.99'
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '1824.64'
        'y': '38.62'
        z: '1790.11'
    - name: Openbase Leftside 3 Cpoint
      id: openbase_leftside_3_Cpoint
      position:
        x: '1826.78'
        'y': '38.31'
        z: '1854.39'
    - name: Openbase Rightside 4 Cpoint
      id: openbase_rightside_4_Cpoint
      position:
        x: '1697.61'
        'y': '38.31'
        z: '1781.07'
  created: '2003-08-13'
  creator: Dnamro
origin: https://www.gamefront.com/games/battlefield-1942/file/gcx-endoria
---

