---
title: Plains Of Sakiya
author: ''
description: >-
  Angered by the defeat of his elite Storm Commandos at the hands of the Sakiyan
  rebels, the Emperor has sent a large attack force to eliminate the rebel
  threat. However, since the initial uprising, the Rebel alliance has given the
  Sakiyan rebels large reinforcments, making them a match for the Empire.
date: ''
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
  size: 2048
  source: extended
  controlpoints:
    - name: Rebel HQ
      id: Rebel_HQ
      position:
        x: '813.22'
        'y': '120.02'
        z: '255.55'
    - name: Imperial HQ
      id: Imperial_HQ
      position:
        x: '1147.60'
        'y': '120.07'
        z: '1679.48'
    - name: Lake Ihciat
      id: Lake_Ihciat
      position:
        x: '1375.36'
        'y': '80.67'
        z: '1009.88'
    - name: Sako Hill
      id: Sako_Hill
      position:
        x: '383.03'
        'y': '123.89'
        z: '857.43'
    - name: Rebel Airbase
      id: Rebel_Airbase
      position:
        x: '1616.88'
        'y': '88.90'
        z: '468.00'
    - name: Imperial Airbase
      id: Imperial_Airbase
      position:
        x: '450.11'
        'y': '91.66'
        z: '1597.40'
    - name: The Forest
      id: The_Forest
      position:
        x: '855.90'
        'y': '91.46'
        z: '1054.42'
---

