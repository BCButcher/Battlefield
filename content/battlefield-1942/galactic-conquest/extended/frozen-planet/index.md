---
title: Frozen Planet
author: Dnamro
description: >-
  Imperials are attacking from the west with all available forces. Rebels defend
  the East side with all Rebel forces available including the M. Falcon.
date: '2004-03-27'
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: extended
  controlpoints:
    - name: South Open Base
      id: SouthOpenBase
      position:
        x: '905.77'
        'y': '86.30'
        z: '582.72'
    - name: North Open Base
      id: NorthOpenBase
      position:
        x: '874.01'
        'y': '51.59'
        z: '1815.98'
    - name: East Open Base
      id: EastOpenBase
      position:
        x: '1343.90'
        'y': '45.40'
        z: '1380.03'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '1694.08'
        'y': '60.00'
        z: '804.90'
    - name: Axis Base
      id: AxisBase
      position:
        x: '451.57'
        'y': '40.20'
        z: '1271.61'
  created: '2004-03-27'
  creator: Dnamro
origin: https://www.gamefront.com/games/battlefield-1942/file/gcx-frozen-planet
---

