---
title: Tanaab
author: Szier, GC Team
description: ''
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Objectives
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Objectives
  size: 4096
  source: original
  controlpoints: []
  created: '2003-03-23'
  creator: Szier, GC Team
origin: https://www.shacknews.com/article/30970/new-galactic-conquest
---

