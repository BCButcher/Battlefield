---
title: Bonus Race
author: GC Team
description: >-
  The fog has lifted for the final day of the 100th Anual Chandrila Racing
  Challenge. The competition has been fierce between the two remaining teams,
  The Galactic Empire and Rebel Alliance. They will need to take advantage of
  newly enhanced skyhopper engine to secure victory in the skys. They will also
  need lighting fast reflexs on our famous speederbike course. Have Fun Racers!
  On your Mark... Get Set... SMACKTARD!!!
date: '2005-04-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Skyhopper I
      id: SkyhopperI
      position:
        x: '1060.06'
        'y': '3.63'
        z: '280.14'
    - name: Skyhopper R
      id: SkyhopperR
      position:
        x: '1060.08'
        'y': '3.63'
        z: '184.03'
    - name: Speederbike I
      id: SpeederbikeI
      position:
        x: '845.76'
        'y': '243.84'
        z: '1725.70'
    - name: Speedbike R
      id: SpeedbikeR
      position:
        x: '893.91'
        'y': '243.64'
        z: '1725.75'
    - name: Imperial Ships
      id: Imperial_Ships
      position:
        x: '2012.74'
        'y': '256.07'
        z: '2023.30'
    - name: Rebel Ships
      id: Rebel_Ships
      position:
        x: '21.11'
        'y': '255.45'
        z: '2013.25'
  created: '2005-04-02'
  creator: GC Team
origin: >-
  http://www.bf-games.net/downloads/299/galactic-conquest-client-v05-full-installer.html
---

