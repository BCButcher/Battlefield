---
title: Mini Cove
author: GC Team
description: >-
  With the invasion of Mon Calamari underway the Empire requires more landmass
  to bring down reinforcements. A small division of troops using captured
  vehicles has broken away from the main battle to capture a small island. The
  Mon Calamari have spotted the advancement and have sent their own troops to
  intercept...
date: '2005-05-22'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Imperial Base
      id: imperial_base
      position:
        x: '189.41'
        'y': '103.57'
        z: '176.35'
    - name: Rebel Base
      id: rebel_base
      position:
        x: '797.99'
        'y': '100.32'
        z: '894.44'
    - name: Imperial Dome
      id: imperial_dome
      position:
        x: '435.32'
        'y': '109.11'
        z: '393.36'
    - name: Rebel Dome
      id: rebel_dome
      position:
        x: '684.63'
        'y': '105.33'
        z: '426.87'
    - name: Supply Platform
      id: Supply_Platform
      position:
        x: '497.12'
        'y': '118.02'
        z: '592.15'
  created: '2005-05-22'
  creator: GC Team
origin: https://www.bf-games.net/forum/topic/19126-galactic-conquest-53/
---

