---
title: Mini Wasteland
author: GC Team
description: >-
  The planet Lok can make even the most well built outpost wither away in its
  vast wastelands. Two forgotten bases have rediscovered each other and plan to
  wipe the other off the face of the planet. Cross the Wasteland to capture and
  defend key locations...
date: '2005-05-22'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Imperial Base
      id: Imperial_Base
      position:
        x: '587.14'
        'y': '109.79'
        z: '871.01'
    - name: Rebel Base
      id: Rebel_Base
      position:
        x: '530.04'
        'y': '111.46'
        z: '210.06'
    - name: Rebel Lowland
      id: Rebel_Lowland
      position:
        x: '717.68'
        'y': '76.21'
        z: '374.61'
    - name: Volcano
      id: Volcano
      position:
        x: '525.54'
        'y': '131.24'
        z: '551.06'
    - name: Imperial Lowland
      id: Imperial_Lowland
      position:
        x: '355.79'
        'y': '76.05'
        z: '732.59'
  created: '2005-05-22'
  creator: GC Team
origin: https://www.bf-games.net/forum/topic/19126-galactic-conquest-53/
---

