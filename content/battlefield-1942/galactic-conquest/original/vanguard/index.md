---
title: Vanguard
author: Szier, GC Team
description: >-
  While the Battle for Endor gets underway, a Vanguard of rebel forces move to
  block a Hyperspace cooridor to Couruscant that could be used by Imperials for
  reinforcments, or provide fleeing Imperial officers an escape route.
date: '2005-04-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: original
  controlpoints:
    - name: Rebebbasetemplate
      id: rebebbasetemplate
      position:
        x: '267.372'
        'y': '347.9'
        z: '266.241'
    - name: Imperail Spawntemplate
      id: imperail_spawntemplate
      position:
        x: '65.2725'
        'y': '371.382'
        z: '3942.74'
    - name: Hyperspace Nav 1 Template
      id: hyperspace_nav1template
      position:
        x: '3092.89'
        'y': '323.057'
        z: '3439.01'
    - name: Field Navpointtemplate
      id: field_navpointtemplate
      position:
        x: '3491.46'
        'y': '206.905'
        z: '766.768'
    - name: Field Entry Navtemplate
      id: field_entry_navtemplate
      position:
        x: '1992.97'
        'y': '339.283'
        z: '2195.91'
  created: '2005-04-02'
  creator: Szier, GC Team
origin: >-
  http://www.bf-games.net/downloads/299/galactic-conquest-client-v05-full-installer.html
---

