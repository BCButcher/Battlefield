---
title: Corellia
author: GC Team
description: >-
  Immediately after the destruction of the Deathstar the Rebel base on Yavin 4
  was abandaned. Imperial forces began desperately searching the galaxy for
  their whereabouts. During the search, Imperial spies identified a ship
  matching the description of the Millenium Falcon leaving Ord Mandell enroute
  to Corellia. The Empire issued a communicade to the Imperial garrisson in Bela
  Vistal on Corellia. Their orders: The Empire has declared Imperial Law over
  the Corellian system. By any means necessary take control of the outlying
  regions and rid Rebel influence once and for all...
date: '2005-04-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Bela Vistal
      id: Bela_Vistal
      position:
        x: '1388.61'
        'y': '66.08'
        z: '1757.99'
    - name: Nerf City
      id: Nerf_City
      position:
        x: '874.33'
        'y': '79.59'
        z: '253.68'
    - name: Vreni Island
      id: Vreni_Island
      position:
        x: '569.40'
        'y': '72.30'
        z: '995.88'
    - name: Daoba Guerfel
      id: Daoba_Guerfel
      position:
        x: '1516.00'
        'y': '76.08'
        z: '988.77'
    - name: Rebel Hideout
      id: Rebel_Hideout
      position:
        x: '1133.83'
        'y': '76.18'
        z: '721.54'
    - name: Imperial Outpost
      id: Imperial_Outpost
      position:
        x: '1074.79'
        'y': '93.16'
        z: '1339.10'
  created: '2005-04-02'
  creator: GC Team
origin: >-
  http://www.bf-games.net/downloads/299/galactic-conquest-client-v05-full-installer.html
---

