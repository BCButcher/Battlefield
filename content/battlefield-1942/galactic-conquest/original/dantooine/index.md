---
title: Dantooine
author: Szier, Luuri, GC Team
description: ''
date: '2004-09-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 4
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Southopenbase
      id: southopenbase
      position:
        x: '907.558'
        'y': '84.21'
        z: '560.728'
    - name: Northopenbase
      id: northopenbase
      position:
        x: '816.142'
        'y': '27.6593'
        z: '1758.12'
    - name: Eastopenbase
      id: eastopenbase
      position:
        x: '1343.67'
        'y': '44.24'
        z: '1423.18'
    - name: Alliesbase
      id: alliesbase
      position:
        x: '1766.52'
        'y': '59.7566'
        z: '780.672'
    - name: Axisbase
      id: axisbase
      position:
        x: '269.845'
        'y': '17.9603'
        z: '1003.31'
  created: '2004-09-11'
  creator: Szier, Luuri, GC Team
origin: https://www.boards.ie/discussion/186606/mod-release-galactic-conquest-0-4
---

