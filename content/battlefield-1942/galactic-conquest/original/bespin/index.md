---
title: Bespin
author: Szier, Luuri, GC Team
description: ''
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Platform
      id: Imperial_Platform
      position:
        x: '772.13'
        'y': '268.30'
        z: '1004.27'
    - name: Rebel Platform
      id: Rebel_Platform
      position:
        x: '1240.58'
        'y': '268.30'
        z: '1004.27'
    - name: Imperial Bridge
      id: Imperial_Bridge
      position:
        x: '881.92'
        'y': '279.83'
        z: '962.78'
    - name: Rebel Tower
      id: Rebel_Tower
      position:
        x: '1147.12'
        'y': '280.30'
        z: '1045.96'
    - name: Central Platform
      id: Central_Platform
      position:
        x: '1006.12'
        'y': '275.50'
        z: '1033.39'
    - name: Court Yard
      id: Court_Yard
      position:
        x: '1006.00'
        'y': '251.68'
        z: '913.11'
  created: '2003-03-23'
  creator: Szier, Luuri, GC Team
origin: https://www.shacknews.com/article/30970/new-galactic-conquest
---

