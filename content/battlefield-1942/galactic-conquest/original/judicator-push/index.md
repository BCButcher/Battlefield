---
title: Judicator Push
author: GC Team
description: ''
date: '2005-05-22'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Bridgetemplate
      id: imperial_bridgetemplate
      position:
        x: '1591.56'
        'y': '590.19'
        z: '881.71'
    - name: Rebelblockadetemplate
      id: rebelblockadetemplate
      position:
        x: '541.45'
        'y': '341.23'
        z: '849.9'
    - name: Lift Controltemplate
      id: lift_controltemplate
      position:
        x: '1642.13'
        'y': '341.4'
        z: '992.66'
    - name: Level 1 Security Roomtemplate
      id: level1_security_roomtemplate
      position:
        x: '557.61'
        'y': '341.4'
        z: '925.22'
    - name: Tram Controltemplate
      id: tram_controltemplate
      position:
        x: '551.205'
        'y': '336.54'
        z: '984.94'
    - name: Checkpoint 1 Template
      id: checkpoint_1template
      position:
        x: '827.77'
        'y': '341.4'
        z: '918.54'
    - name: Checkpoint 3 Template
      id: checkpoint_3template
      position:
        x: '1042.54'
        'y': '341.35'
        z: '885.08'
    - name: Checkpoint 6 Template
      id: checkpoint_6template
      position:
        x: '1431.43'
        'y': '341.41'
        z: '867.99'
  created: '2005-05-22'
  creator: GC Team
origin: https://www.bf-games.net/forum/topic/19126-galactic-conquest-53/
---

