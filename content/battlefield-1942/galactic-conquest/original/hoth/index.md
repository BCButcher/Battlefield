---
title: Hoth
author: Liam, Szier, GC Team
description: >-
  After the falling of the first death star, the Rebel Alliance stationed their
  forces on an ice planet surrounded by an asteroid belt. Hoth is both flat and
  mountainous with a network of caves covered by thick snow, very often do
  asteroids pelt the surface creating craters, before covering over with snow
  from the harsh snowstorms, making it an ideal base of operations. With the
  Empire despatching several thousand probe droids, they eventually found signs
  of the Rebel Alliance inhabiting a mountain base on the planet Hoth. Once
  found the Empire launched a full scale ground invasion triggering the Battle
  of Hoth.


  **Terrain**: Mountainous & Large snowy plains


  **Inhabitants**: Taun Tauns and Wampa's


  **Occupants**: Rebel Alliance
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Echobasecp
      id: echobasecp
      position:
        x: '335.32'
        'y': '2.84'
        z: '325.74'
    - name: Shieldgeneratorcp
      id: shieldgeneratorcp
      position:
        x: '1124.53'
        'y': '16.35'
        z: '478.91'
    - name: Secondlinetrench
      id: secondlinetrench
      position:
        x: '520.74'
        'y': '14.17'
        z: '652.14'
    - name: Firstlinetrench
      id: firstlinetrench
      position:
        x: '987.32'
        'y': '16.27'
        z: '841.58'
    - name: Imperialspawn
      id: imperialspawn
      position:
        x: '1404.69'
        'y': '21.48'
        z: '1740.63'
    - name: Thirdlinetrench
      id: thirdlinetrench
      position:
        x: '636.13'
        'y': '14.08'
        z: '423.73'
  created: '2003-03-23'
  creator: Liam, Szier, GC Team
origin: >-
  https://web.archive.org/web/20030829074346/http://www.galactic-conquest.net/index.php?page=&action=show&id=2189
---

