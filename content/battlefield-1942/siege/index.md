---
title: Siege
name: Siege
description: >-
  Siege: Medieval Warfare is a medieval combat modification for the computer
  game Battlefield 1942. It is set during the 100 Years War between the Kingdoms
  of France and England. Fight as a common soldier on open fields or assault a
  castle using a large array of medieval weaponry and vehicles.


  Siege: Medieval Warfare aims to bring an entire new experience to the somewhat
  out-dated Battlefield 1942. This mod will bring both medieval realism and
  exciting game-play while keeping a balance between the two.
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
origin: https://www.moddb.com/mods/siege-medieval-warfare
banner: true
---

