---
title: Sword And Shield
author: ''
description: Defend the Village!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: French Base
      id: FrenchBase
      position:
        x: '487.00'
        'y': '77.20'
        z: '324.00'
    - name: English Base
      id: EnglishBase
      position:
        x: '516.00'
        'y': '76.15'
        z: '655.00'
    - name: Village
      id: Village
      position:
        x: '496.00'
        'y': '76.15'
        z: '488.00'
    - name: Small House
      id: Small_House
      position:
        x: '502.00'
        'y': '76.20'
        z: '410.00'
    - name: Bridge
      id: Bridge
      position:
        x: '521.50'
        'y': '81.20'
        z: '571.00'
    - name: Carneval
      id: Carneval
      position:
        x: '548.56'
        'y': '76.19'
        z: '474.89'
---

