---
title: Helms Deep
author: ''
description: 'Remake: The Battle of Helm''s Deep.'
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Main Gate
      id: Main_gate
      position:
        x: '475.00'
        'y': '145.50'
        z: '570.50'
    - name: Inner Fort
      id: Inner_Fort
      position:
        x: '438.42'
        'y': '135.90'
        z: '630.20'
    - name: Cave
      id: Cave
      position:
        x: '388.55'
        'y': '146.50'
        z: '620.50'
---

