---
title: Crossing
author: ''
description: Get the statue!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: French Base
      id: French_Base
      position:
        x: '455.90'
        'y': '121.00'
        z: '192.00'
    - name: British Base
      id: British_Base
      position:
        x: '478.00'
        'y': '126.50'
        z: '805.00'
    - name: Angel
      id: Angel
      position:
        x: '490.00'
        'y': '126.30'
        z: '512.00'
---

