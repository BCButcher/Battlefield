---
title: Battle Of Agincourt
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: English Base
      id: EnglishBase
      position:
        x: '589.19'
        'y': '139.25'
        z: '382.51'
    - name: French Base
      id: FrenchBase
      position:
        x: '292.81'
        'y': '76.89'
        z: '564.09'
    - name: Stake Entrenchment
      id: Stake_Entrenchment
      position:
        x: '453.79'
        'y': '94.56'
        z: '444.63'
---

