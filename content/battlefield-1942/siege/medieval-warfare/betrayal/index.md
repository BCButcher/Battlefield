---
title: Betrayal
author: ''
description: The main gate is open! Attack!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Castle
      id: Castle
      position:
        x: '455.82'
        'y': '126.90'
        z: '487.99'
    - name: Front
      id: Front
      position:
        x: '482.63'
        'y': '134.70'
        z: '432.33'
---

