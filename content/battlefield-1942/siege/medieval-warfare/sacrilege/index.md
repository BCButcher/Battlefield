---
title: Sacrilege
author: ''
description: Hold the Church!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Church
      id: Church
      position:
        x: '529.20'
        'y': '112.00'
        z: '505.50'
---

