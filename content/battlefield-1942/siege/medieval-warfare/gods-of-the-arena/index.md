---
title: Gods Of The Arena
author: ''
description: Glory!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: French Base
      id: FrenchBase
      position:
        x: '520.00'
        'y': '137.60'
        z: '571.00'
    - name: English Base
      id: EnglishBase
      position:
        x: '500.00'
        'y': '143.50'
        z: '474.50'
    - name: Glory
      id: Glory
      position:
        x: '512.10'
        'y': '137.58'
        z: '524.69'
---

