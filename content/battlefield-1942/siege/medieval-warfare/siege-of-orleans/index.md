---
title: Siege Of Orleans
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Tourelles Fortress
      id: Tourelles_Fortress
      position:
        x: '453.26'
        'y': '26.58'
        z: '276.92'
    - name: River Fort
      id: River_Fort
      position:
        x: '411.68'
        'y': '15.17'
        z: '429.41'
    - name: Barbicon
      id: Barbicon
      position:
        x: '460.65'
        'y': '11.62'
        z: '244.36'
    - name: St Augustine
      id: St_Augustine
      position:
        x: '462.36'
        'y': '12.54'
        z: '146.97'
    - name: Orleans
      id: Orleans
      position:
        x: '479.31'
        'y': '11.19'
        z: '554.46'
    - name: St Jean The White
      id: St_Jean_The_White
      position:
        x: '812.35'
        'y': '12.68'
        z: '113.23'
---

