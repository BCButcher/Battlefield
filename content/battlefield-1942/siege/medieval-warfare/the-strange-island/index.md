---
title: The Strange Island
author: ''
description: Une ile �trange en ruine peupl�e de crapauds....
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Ile
      id: Ile
      position:
        x: '516.83'
        'y': '84.27'
        z: '492.87'
    - name: New Control Point 1
      id: New_Control_Point1
      position:
        x: '19.13'
        'y': '79.67'
        z: '13.14'
---

