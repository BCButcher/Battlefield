---
title: Castle Ruin
author: ''
description: .
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: medieval-warfare
  controlpoints:
    - name: Allied Base
      id: AlliedBase
      position:
        x: '1132.39'
        'y': '76.11'
        z: '1030.22'
    - name: Church
      id: Church
      position:
        x: '1711.04'
        'y': '99.71'
        z: '1050.68'
    - name: Houses
      id: Houses
      position:
        x: '1626.57'
        'y': '99.84'
        z: '1058.40'
    - name: Tente
      id: tente
      position:
        x: '1714.49'
        'y': '102.73'
        z: '999.96'
---

