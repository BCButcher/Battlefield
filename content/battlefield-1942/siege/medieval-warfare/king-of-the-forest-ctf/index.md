---
title: King Of The Forest Ctf
author: ''
description: Capture the Flag!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: English Flag
      id: English_Flag
      position:
        x: '451.00'
        'y': '83.70'
        z: '315.00'
    - name: French Flag
      id: French_Flag
      position:
        x: '336.78'
        'y': '85.02'
        z: '428.09'
---

