---
title: Mount St Michael
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '414.55'
        'y': '291.04'
        z: '410.93'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '435.04'
        'y': '240.94'
        z: '200.77'
    - name: Courtyard
      id: courtyard
      position:
        x: '455.91'
        'y': '270.82'
        z: '408.60'
    - name: Town
      id: Town
      position:
        x: '483.51'
        'y': '248.24'
        z: '425.09'
---

