---
title: Weserubung
author: TA_Radar & KB
description: >-
  Hitler began planning the occupation of Norway and Denmark under the code-name
  "Weserübung," or "Wesser Exercise." His original plans called for a neutral
  Scandinavia, but after time he feared northern Europe could be used against
  him. Consequently he sought to establish his forces there before the Allies,
  who had their own plans in the region. A British-controlled Norwegian
  coastline would directly threaten German positions along the Baltic Sea, as
  well as impose a costly blockade. Hitler wanted Norway for its valuable
  resources, especially its iron ore, a necessary war material, which was
  exported from the northern Norwegian port of Narvik.


  Norway would also serve as the perfect strategic naval and aerial base to
  attack England and Hitler boasted that "weakness in numbers will be made good
  by skillful action and surprise in execution." For the most part, Hitler was
  right. The Norwegian armed forces, later headed by Maj. Gen. Otto Ruge, would
  not be able to defend such a long stretch of coastline. The Germans planned to
  make them surrender by gaining control of the Norwegian ports and the capital
  city of Oslo. Britain had a similar plan, but was designed to protect Norway
  from the Germans. Initially the British were unsure of what course of action
  to take, and decided to mine the waters between Norway and its offshore
  islands, known as the "Norwegian Leads." Dubbed "Operation Wilfred," this
  would prevent German ships from sneaking through neutral waterways.


  The British also planned to send in troops to secure the ports and convert the
  port city of Narvik to an Allied naval base. They told Norway and Sweden if
  they allowed the Germans to use Scandinavian sealanes for any reason that they
  would lose their neutrality status. This was something to consider because
  both countries wanted desperately to stay neutral like they did in the First
  World War. The German ships sailed on 7 April 1940, the same day the British
  ships set sail to mine the Norwegian waterways, and mining began the following
  day.


  Under the command of General Nicholas von Falkenhorst, the Germans were set
  for an invasion with approximately 9,000 troops aboard 71 ships masquerading
  as British warships to confuse the Norwegians. Rumors of the invasion swept
  across Scandinavia, but each country assumed it would happen to another
  country and not them. Britain also misread the German naval build up as an
  attempt to break through their blockade, and ignored several signs that
  indicated the Germans' intentions. Several naval battles then ensued, one
  especially notable on 8 April 1940 when the diminutive British destroyer
  Glowworm was mauled by the massive German cruiser Admiral Hipper.


  Rather than sink gracefully, the Glowworm turned full steam into the Admiral
  Hipper in a last ditch effort to avenge her inevitable death. The Glowworm was
  survived by only 31 sailors, but the Admiral Hipper was damaged enough to make
  her return to port. Two days later another German cruiser, Bülcher, was taken
  by surprise by the Norwegians and sunk. The Lützow behind it saw the ship
  explode violently and assumed there was a minefield ahead, so she turned
  around, calling off the attack on Oslo. On land, the German attacks were
  vastly more successful and the Germans stormed the cities of Bergin,
  Kristiansand, Narvik, Oslo and Trondheim. King Haakon VII, the Norwegian Royal
  family, and most of the government fled as Hitler demanded that a fascist
  government be established to run the country.


  The King said he would rather abdicate, but an ambitious pro-Nazi Norwegian
  politician, Vidkun Quisling, helped Hitler with his invasion by proclaiming
  himself the new President. The Norwegians despised him (making sure he was
  executed when the war was over) and his name became synonymous with "traitor,"
  much like Benedict Arnold in American history. The Germans defended their
  actions by claiming they were "saving" Norway from an Allied occupation when
  in fact they became the occupying army.


  ## Features


  This Battlefield map is losely based on Operation Weserübung, and takes place
  outside Narvik in Norway. It does not intend to be historically correct, but
  instead it aims for balanced gameplay and gives an advanced tactical aspect
  which has not been explored in the regular Battlefield maps. The map seems
  pretty big, yet it will not take the player long to drive to the next point.
  It is a mix between open terrain and Berlin-style fighting because of the
  cities.


  The terrain in the map is rough and shaped so that a good sniper will be in
  heaven, but any other class can hide just by proning anywhere in the terrain.
  The terrain also makes room for a lot of ambush points in the map, and the mix
  between the smooth roads and the rough terrain gives the vehicles their real
  roles. Jeeps drive shitty in the terrain, while tanks and APCs can drive up
  the mountain if they wish to. All classes and vehicles have very separate
  advantages in this map.


  Except for the harbor, the whole island is shallow around the shores. The
  naval has to battle it out on the sea or around the harbor, like they have to
  do in real life. The only way for the ships to kill a target on shore is by
  snipers giving them artillery targets.
date: '2003-06-18'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: custom
  controlpoints:
    - name: Main Town
      id: Main_Town
      position:
        x: '404.056610'
        'y': '63.549999'
        z: '920.546204'
    - name: North Town
      id: North_Town
      position:
        x: '877.318604'
        'y': '48.002300'
        z: '1182.514160'
    - name: South Town
      id: South_Town
      position:
        x: '873.138489'
        'y': '48.599998'
        z: '713.871216'
    - name: Harbor
      id: Harbor
      position:
        x: '1407.225586'
        'y': '48.000000'
        z: '854.045227'
  created: '2003-06-18'
  creator: TA_Radar & KB
origin: https://www.gamefront.com/games/battlefield-1942/file/operation-weseruebung
---

