---
title: Battlefield 1942
name: Original
description: >-
  The game is a First Person Shooter with many assignments spread over four
  campaigns. Several players can fight online on wide battle fields and control
  airplanes, tanks, and aircraft carriers. The weapons are easy to handle, but
  to be successful in Battlefield 1942 you must be cunning and have stamina.

  Battlefield 1942 contains a specially produced game engine that handles
  dynamic models, scenery, land and air physics, and includes a system for 3D
  sound that provides an unrivalled feeling of presence. Suddenly, you are just
  there! If you like action-filled games for several players, don't miss
  Battlefield 1942.
categories:
  - Battlefield 1942
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20030721201555/http://global.dice.se/games/battlefield/
---

