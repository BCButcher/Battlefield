---
title: Battle of Midway
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - TDM
  size: 4096
  source: original
  controlpoints:
    - name: The Airfield
      id: The_Airfield
      position:
        x: '2078.63'
        'y': '24.3187'
        z: '2112.66'
    - name: The Radar Bunker
      id: The_Radar_Bunker
      position:
        x: '1836.99'
        'y': '32.9391'
        z: '1972.83'
    - name: North Midway
      id: North_Midway
      position:
        x: '2030.71'
        'y': '19.9763'
        z: '3055.48'
    - name: South Midway
      id: South_Midway
      position:
        x: '2033.56'
        'y': '19.8993'
        z: '1032.48'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

