---
title: Liberation Of Caen
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Canadian Base
      id: Canadian_Base
      position:
        x: '979.942'
        'y': '34.9532'
        z: '632.465'
    - name: South River
      id: South_River
      position:
        x: '1232.66'
        'y': '35.7376'
        z: '1059.46'
    - name: Pegasus Bridge CP
      id: Pegasus_Bridge_CP
      position:
        x: '969.703'
        'y': '35.1749'
        z: '1255.0'
    - name: Church
      id: Church
      position:
        x: '892.164'
        'y': '38.1422'
        z: '1401.43'
    - name: Park
      id: Park
      position:
        x: '1067.85'
        'y': '34.4062'
        z: '1421.37'
    - name: German Headquarters
      id: German_Headquarters
      position:
        x: '978.91'
        'y': '36.31'
        z: '1480.41'
---

