---
title: Operation Battleaxe
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Controlpoint Allies Base
      id: controlpoint_allies_base
      position:
        x: '1318.97'
        'y': '63.1867'
        z: '672.199'
    - name: Controlpoint Allies Right
      id: controlpoint_allies_right
      position:
        x: '1467.31'
        'y': '35.8101'
        z: '895.912'
    - name: Controlpoint Allies Middle
      id: controlpoint_allies_middle
      position:
        x: '1407.47'
        'y': '35.9438'
        z: '840.288'
    - name: Controlpoint Allies Left
      id: controlpoint_allies_left
      position:
        x: '1321.2'
        'y': '35.7562'
        z: '890.967'
    - name: Controlpoint Axis Base
      id: controlpoint_axis_base
      position:
        x: '1365.13'
        'y': '63.2788'
        z: '1183.62'
    - name: Controlpoint Axis Right
      id: controlpoint_axis_right
      position:
        x: '1326.36'
        'y': '35.9047'
        z: '1004.26'
    - name: Controlpoint Axis Middle
      id: controlpoint_axis_middle
      position:
        x: '1397.13'
        'y': '36.2707'
        z: '1050.91'
    - name: Controlpoint Axis Left
      id: controlpoint_axis_left
      position:
        x: '1461.65'
        'y': '36.1307'
        z: '1001.12'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

