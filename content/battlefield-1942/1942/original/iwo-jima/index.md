---
title: Iwo Jima
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: original
  controlpoints:
    - name: Top BASE Cpoint
      id: TopBASE_Cpoint
      position:
        x: '652.584'
        'y': '93.0164'
        z: '1420.88'
    - name: Big BASE Cpoint
      id: BigBASE_Cpoint
      position:
        x: '1101.51'
        'y': '77.4669'
        z: '918.225'
    - name: BUNKERS Cpoint
      id: BUNKERS_Cpoint
      position:
        x: '888.909'
        'y': '79.6713'
        z: '1136.62'
    - name: Sand Base Cpoint
      id: sandBase_Cpoint
      position:
        x: '818.321'
        'y': '53.695'
        z: '1076.1'
    - name: Midbase Suribashibottom
      id: midbase_suribashibottom
      position:
        x: '866.168'
        'y': '74.4739'
        z: '1217.1'
    - name: Top BASE 2 Cpoint
      id: TopBASE2_Cpoint
      position:
        x: '1085.93'
        'y': '105.134'
        z: '772.249'
    - name: Top BASE 3 Cpoint
      id: TopBASE3_Cpoint
      position:
        x: '1228.22'
        'y': '109.711'
        z: '911.883'
    - name: ALLIE Sstart 1 Cpoint
      id: ALLIESstart1_Cpoint
      position:
        x: ''
    - name: ALLIE Sstart 2 Cpoint
      id: ALLIESstart2_Cpoint
      position:
        x: ''
---

