---
title: Wake Island
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
  size: 2048
  source: original
  controlpoints:
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '1383.75'
        'y': '115.998'
        z: '775.193'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

