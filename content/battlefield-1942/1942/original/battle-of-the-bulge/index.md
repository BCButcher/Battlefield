---
title: Battle Of The Bulge
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '824.687'
        'y': '56.3022'
        z: '1076.51'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '669.938'
        'y': '73.9325'
        z: '307.123'
    - name: Cammobunker
      id: cammobunker
      position:
        x: '983.121'
        'y': '54.9258'
        z: '764.564'
    - name: City
      id: city
      position:
        x: '860.903'
        'y': '59.871'
        z: '579.522'
    - name: Bridgebunker
      id: bridgebunker
      position:
        x: '649.656'
        'y': '59.611'
        z: '623.812'
    - name: Windmill
      id: windmill
      position:
        x: '748.828'
        'y': '82.4536'
        z: '878.127'
---

