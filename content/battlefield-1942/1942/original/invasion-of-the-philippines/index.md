---
title: Invasion Of The Philippines
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: US Marine Base
      id: US_Marine_Base
      position:
        x: '547.1577'
        'y': '77.0391'
        z: '1237.35'
    - name: West Harbor
      id: West_Harbor
      position:
        x: '558.531'
        'y': '48.9165'
        z: '778.313'
    - name: Airfield
      id: Airfield
      position:
        x: '1088.8751'
        'y': '55.4227'
        z: '1115.8848'
    - name: Point Boyington
      id: Point_Boyington
      position:
        x: '1134.15'
        'y': '56.0565'
        z: '1333.57'
    - name: East Harbor
      id: 'East_Harbor '
      position:
        x: '1567.99'
        'y': '61.179'
        z: '1502.55'
    - name: Islands
      id: 'Islands '
      position:
        x: '836.044'
        'y': '45.3741'
        z: '1003.18'
---

