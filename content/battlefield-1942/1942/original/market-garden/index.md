---
title: Market Garden
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: original
  controlpoints:
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '1060.72'
        'y': '50.3485'
        z: '1704'
    - name: Open Spawn Point Alliedside Ironbrdgbunker
      id: OpenSpawnPoint_alliedside_ironbrdgbunker
      position:
        x: '1201.4'
        'y': '40.647'
        z: '1221.42'
    - name: Open Spawn Point Alliedside Niceblock
      id: OpenSpawnPoint_alliedside_niceblock
      position:
        x: '1090.42'
        'y': '34.3399'
        z: '1250.71'
    - name: Open Spawn Point Alliedside Church
      id: OpenSpawnPoint_alliedside_church
      position:
        x: '1122'
        'y': '34.3594'
        z: '1417.07'
    - name: Open Spawn Point Alliedside Stnbrdgbunker
      id: OpenSpawnPoint_alliedside_stnbrdgbunker
      position:
        x: '965.38'
        'y': '38.6109'
        z: '1163.68'
    - name: Open Spawn Point Axisside Ironbrdgbunker
      id: OpenSpawnPoint_axisside_ironbrdgbunker
      position:
        x: '1132.64'
        'y': '41.1703'
        z: '930.198'
    - name: Open Spawn Point Axisside Stnbrgbunker
      id: OpenSpawnPoint_axisside_stnbrgbunker
      position:
        x: '969.685'
        'y': '40.6406'
        z: '998.004'
    - name: AXIS BASE
      id: AXIS_BASE
      position:
        x: '1060.79'
        'y': '34.7619'
        z: '621.275'
    - name: Open Spawn Point Alliedside Gettoblock
      id: OpenSpawnPoint_alliedside_gettoblock
      position:
        x: '1246.43'
        'y': '34.3608'
        z: '1300.97'
---

