---
title: Berlin
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '1824.64'
        'y': '38.618'
        z: '1790.11'
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: '1799.46'
        'y': '38.7'
        z: '1960.99'
    - name: Openbase Leftside 3 Cpoint
      id: openbase_leftside_3_Cpoint
      position:
        x: '1826.78'
        'y': '38.306'
        z: '1854.39'
    - name: Openbase Rightside 4 Cpoint
      id: openbase_rightside_4_Cpoint
      position:
        x: '1697.61'
        'y': '38.306'
        z: '1781.07'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

