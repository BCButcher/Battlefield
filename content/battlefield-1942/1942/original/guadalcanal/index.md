---
title: Guadalcanal
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 4096
  source: original
  controlpoints:
    - name: AXISBASE Cpoint
      id: AXISBASE_Cpoint
      position:
        x: '2420.05'
        'y': '91.8'
        z: '1268.33'
    - name: ALLIES Base 2 Cpoint
      id: ALLIESBase2_Cpoint
      position:
        x: '1515.65'
        'y': '79.2'
        z: '2544.08'
    - name: Axis Bunker Cpoint
      id: Axis_Bunker_Cpoint
      position:
        x: '2199.21'
        'y': '116.923'
        z: '1607.81'
    - name: Openbase 4 Cpoint
      id: openbase4_Cpoint
      position:
        x: '2092.45'
        'y': '81.6'
        z: '1901.82'
    - name: Allied Bunker Cpoint
      id: Allied_Bunker_Cpoint
      position:
        x: '1717.77'
        'y': '116.49'
        z: '2107.74'
    - name: Openbase 6 Cpoint
      id: openbase6_Cpoint
      position:
        x: '1766.45'
        'y': '89.3572'
        z: '1722.48'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

