---
title: Battle Of Britain
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - Objectives
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - Objectives
  size: 2048
  source: original
  controlpoints:
    - name: Axis East Airfield
      id: Axis_East_Airfield
      position:
        x: '340.081'
        'y': '82.2'
        z: '271.095'
    - name: Allied Base
      id: Allied_Base
      position:
        x: '1207.21'
        'y': '104.742'
        z: '1692.75'
---

