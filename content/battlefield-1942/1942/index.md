---
title: Battlefield 1942
name: '1942'
description: >-
  In Battlefield 1942 you fight the Second World War's historic battles when the
  Axis Powers meet the Allies in different parts of the world. You fight the
  battles of Midway, the D-Day invasion in Normandy, and the war in North Africa
  and Japan where millions of people fight and give their lives on land, at sea,
  and in the air.
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
origin: >-
  https://web.archive.org/web/20030721201555/http://global.dice.se/games/battlefield/
banner: true
---

