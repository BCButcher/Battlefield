---
title: Desertshield
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
  size: 2048
  source: final
  controlpoints:
    - name: US CTF Control Point
      id: US_CTF_Control_Point
      position:
        x: '804.59'
        'y': '79.96'
        z: '375.59'
    - name: Iraqi CTF Control Point
      id: Iraqi_CTF_Control_Point
      position:
        x: '938.27'
        'y': '103.66'
        z: '1716.22'
---

