---
title: Operation Bragg
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: final
  controlpoints:
    - name: Us Airbase
      id: us_airbase
      position:
        x: '430.947'
        'y': '15.5793'
        z: '366.702'
    - name: Us Armor Hq
      id: us_armor_hq
      position:
        x: '1109.62'
        'y': '40.1908'
        z: '708.916'
    - name: Oil Pump Station
      id: oil_pump_station
      position:
        x: '513.791'
        'y': '13.8578'
        z: '938.708'
    - name: Jalibah
      id: jalibah
      position:
        x: '1030.91'
        'y': '41.1996'
        z: '1669.39'
    - name: Khamisiyah
      id: khamisiyah
      position:
        x: '1091.01'
        'y': '40.2372'
        z: '1201.9'
---

