---
title: Basrah Nights
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 256
  source: final
  controlpoints:
    - name: Centerspawn
      id: centerspawn
      position:
        x: '384'
        'y': '15.04461'
        z: '638'
    - name: U Sspawn
      id: USspawn
      position:
        x: '352'
        'y': '7.656516'
        z: '560'
    - name: Iraqspawn
      id: iraqspawn
      position:
        x: '418'
        'y': '25.39409'
        z: '720'
    - name: Leftspawn
      id: leftspawn
      position:
        x: '288'
        'y': '15.31158'
        z: '640'
    - name: Rightspawn
      id: rightspawn
      position:
        x: '480'
        'y': '15.12558'
        z: '640'
---

