---
title: Market Garden
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: final
  controlpoints:
    - name: Allies Base
      id: allies_base
      position:
        x: '1060.72'
        'y': '50.3485'
        z: '1704'
    - name: Openspawnpoint Alliedside Ironbrdgbunker
      id: openspawnpoint_alliedside_ironbrdgbunker
      position:
        x: '1201.4'
        'y': '40.647'
        z: '1221.42'
    - name: Openspawnpoint Alliedside Niceblock
      id: openspawnpoint_alliedside_niceblock
      position:
        x: '1090.42'
        'y': '34.3399'
        z: '1250.71'
    - name: Openspawnpoint Alliedside Church
      id: openspawnpoint_alliedside_church
      position:
        x: '1122'
        'y': '34.3594'
        z: '1417.07'
    - name: Openspawnpoint Alliedside Stnbrdgbunker
      id: openspawnpoint_alliedside_stnbrdgbunker
      position:
        x: '965.38'
        'y': '38.6109'
        z: '1163.68'
    - name: Openspawnpoint Axisside Ironbrdgbunker
      id: openspawnpoint_axisside_ironbrdgbunker
      position:
        x: '1132.64'
        'y': '41.1703'
        z: '930.198'
---

