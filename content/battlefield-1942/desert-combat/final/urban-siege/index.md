---
title: Urban Siege
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: final
  controlpoints:
    - name: Iraqi Hq
      id: Iraqi_Hq
      position:
        x: '257.09'
        'y': '80.68'
        z: '219.64'
    - name: Docks
      id: Docks
      position:
        x: '1017.53'
        'y': '78.88'
        z: '1293.9'
    - name: Market
      id: Market
      position:
        x: '1181.85'
        'y': '88.9'
        z: '877.12'
    - name: Trade Center
      id: TradeCenter
      position:
        x: '875.98'
        'y': '81.07'
        z: '1068.16'
    - name: Down Town
      id: DownTown
      position:
        x: '989.4'
        'y': '76.86'
        z: '905.14'
    - name: Highrise
      id: Highrise
      position:
        x: '764.6985'
        'y': '70.4'
        z: '682.1866'
    - name: Tower
      id: Tower
      position:
        x: '1000'
        'y': '79.27'
        z: '1044'
---

