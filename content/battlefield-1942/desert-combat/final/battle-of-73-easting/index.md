---
title: Battle Of 73 Easting
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: final
  controlpoints:
    - name: Norfolk
      id: norfolk
      position:
        x: '280.00'
        'y': '25.20'
        z: '720.00'
    - name: 73 Rdeastings
      id: 73rdeastings
      position:
        x: '622.00'
        'y': '25.20'
        z: '381.00'
    - name: Alliesbase
      id: alliesbase
      position:
        x: '180.00'
        'y': '22.80'
        z: '120.00'
    - name: Axisbase
      id: axisbase
      position:
        x: '807.10'
        'y': '25.20'
        z: '866.31'
---

