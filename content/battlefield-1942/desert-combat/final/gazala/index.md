---
title: Gazala
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 0
  source: final
  controlpoints: []
  created: '2004-02-04'
---

