---
title: Coastal Hammer
author: ''
description: >-
  OPERATION COASTAL HAMMER: An amphibious brigade of Iraqi mechanized infantry
  has been tasked with the invasion and re-capture of a coastal town currently
  occupied by Coalition forces. The invasion is to be launched from a staging
  area on a small off-shore island. The coalition side must hold all changeable
  control points, while the opposition must capture four to achieve enemy ticket
  bleed.
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: final
  controlpoints:
    - name: Forest Base
      id: Forest_Base
      position:
        x: '1664.13'
        'y': '128.21'
        z: '108.28'
    - name: Beach Base
      id: Beach_Base
      position:
        x: '1048.82'
        'y': '131.61'
        z: '693.68'
    - name: Town CTF
      id: Town_CTF
      position:
        x: '971.02'
        'y': '164.98'
        z: '154.29'
    - name: Island CTF
      id: Island_CTF
      position:
        x: '1027.60'
        'y': '124.64'
        z: '1323.81'
---

