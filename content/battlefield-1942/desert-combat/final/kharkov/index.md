---
title: Kharkov
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 0
  source: final
  controlpoints:
    - name: AXIS BASE
      id: 'AXIS_BASE '
      position:
        x: '732.925'
        'y': '92.8996'
        z: '172.094'
    - name: RUSSIAN BASE
      id: RUSSIAN_BASE
      position:
        x: '221.946'
        'y': '92.8949'
        z: '270.447'
    - name: AXIS CONTROLPOINT CITY
      id: AXIS_CONTROLPOINT_CITY
      position:
        x: '662.291'
        'y': '102.996'
        z: '502.494'
    - name: RUSSIAN CONTROLPOINT CITY
      id: 'RUSSIAN_CONTROLPOINT_CITY '
      position:
        x: '370.002'
        'y': '102.995'
        z: '524.828'
    - name: CONTROLPOINT Kharkov
      id: 'CONTROLPOINT_Kharkov '
      position:
        x: '505.123'
        'y': '119.585'
        z: '416.283'
  created: '2004-02-04'
---

