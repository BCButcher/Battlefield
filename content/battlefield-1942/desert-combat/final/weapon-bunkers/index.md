---
title: Weapon Bunkers
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: final
  controlpoints:
    - name: Opposition Camp
      id: OppositionCamp
      position:
        x: '475.612305'
        'y': '74.289803'
        z: '740.222229'
    - name: Coallition Base
      id: CoallitionBase
      position:
        x: '530.610779'
        'y': '74.521896'
        z: '388.674011'
---

