---
title: Cornered
author: ''
description: >-
  COALITION: Recon has shown a light defense at an Iraqi military checkpoint.
  This area is critical to the coalitions further advance into Iraq. OPPOSITION:
  Our checkpoint has been free of the infidels for weeks. We shot down a C-130
  last week, for this glorious defense of Iraq, Saddam will favor us.
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: final
  controlpoints:
    - name: Iraq Airfield
      id: iraq_airfield
      position:
        x: '572.25'
        'y': '94.42'
        z: '665.86'
    - name: Bravo Battery
      id: bravo_battery
      position:
        x: '174.66'
        'y': '84.47'
        z: '835.75'
    - name: Us Camp
      id: us_camp
      position:
        x: '151.00'
        'y': '81.26'
        z: '270.18'
    - name: Iraq Final Retreat
      id: iraq_final_retreat
      position:
        x: '930.11'
        'y': '94.55'
        z: '776.12'
    - name: Iraq 1st Retreat
      id: iraq_1st_retreat
      position:
        x: '893.53'
        'y': '102.41'
        z: '596.69'
    - name: Riverbed
      id: riverbed
      position:
        x: '600.10'
        'y': '77.79'
        z: '377.90'
---

