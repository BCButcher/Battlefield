---
title: Lostvillage
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: final
  controlpoints:
    - name: Alliedbase
      id: alliedbase
      position:
        x: '854.88'
        'y': '76.8'
        z: '696.11'
    - name: Eastpoint
      id: eastpoint
      position:
        x: '1022.1'
        'y': '71.95'
        z: '1003.6'
    - name: Slums
      id: slums
      position:
        x: '1105.67'
        'y': '72'
        z: '1056.09'
    - name: Westpoint
      id: westpoint
      position:
        x: '1117.46'
        'y': '72'
        z: '957.86'
    - name: Villas
      id: villas
      position:
        x: '1180.6'
        'y': '71.95'
        z: '1045.91'
    - name: Axisbase
      id: axisbase
      position:
        x: '1222.74'
        'y': '71.98'
        z: '919.42'
    - name: Factory
      id: factory
      position:
        x: '998.8'
        'y': '71.99'
        z: '1157.91'
---

