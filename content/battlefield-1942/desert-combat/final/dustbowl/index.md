---
title: Dustbowl
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: final
  controlpoints:
    - name: Opposition HQ
      id: OppositionHQ
      position:
        x: '980.45'
        'y': '87.2'
        z: '1792.49'
    - name: Coalition HQ
      id: CoalitionHQ
      position:
        x: '1004.08'
        'y': '87.15'
        z: '274.51'
    - name: South Check Point
      id: SouthCheckPoint
      position:
        x: '1059.46'
        'y': '72.43'
        z: '759.72'
    - name: North Check Point
      id: NorthCheckPoint
      position:
        x: '897.86'
        'y': '77.59'
        z: '1390.07'
    - name: North Outpost
      id: NorthOutpost
      position:
        x: '813.78'
        'y': '50.81'
        z: '1164.41'
    - name: South Outpost
      id: SouthOutpost
      position:
        x: '1143.42'
        'y': '50.97'
        z: '1012.51'
---

