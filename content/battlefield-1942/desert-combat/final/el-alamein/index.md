---
title: El Alamein
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 0
  source: final
  controlpoints:
    - name: South Open Base
      id: SouthOpenBase
      position:
        x: '905.766'
        'y': '86.3'
        z: '582.717'
    - name: North Open Base
      id: NorthOpenBase
      position:
        x: '874.005'
        'y': '51.5906'
        z: '1815.98'
    - name: East Open Base
      id: EastOpenBase
      position:
        x: '1343.9'
        'y': '45.3961'
        z: '1380.03'
---

