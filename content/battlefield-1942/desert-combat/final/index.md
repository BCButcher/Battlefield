---
title: Desert Combat Final
name: Final
description: >-
  Bringing an end to development of Desert Combat is the release of Desert
  Combat Final, a semi-official - Trauma Studios endorsed - update to the
  massively popular Battlefield 1942 mod.
categories:
  - Battlefield 1942
  - Desert Combat
type: source
layout: layouts/source.njk
origin: https://www.ign.com/articles/2004/10/17/desert-combat-final-released
---

