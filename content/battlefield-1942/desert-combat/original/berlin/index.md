---
title: Berlin
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 512
  source: original
  controlpoints: []
  created: '2004-02-04'
---

