---
title: Al Khafji Docks
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Iraqi CTF Controllpoint
      id: IraqiCTFControllpoint
      position:
        x: '674.29'
        'y': '79.59'
        z: '571.48'
    - name: USCTF Controllpoint
      id: USCTFControllpoint
      position:
        x: '453.47'
        'y': '79.59'
        z: '572.43'
---

