---
title: No Fly Zone Day2
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: original
  controlpoints:
    - name: Opposition Airbase
      id: Opposition_Airbase
      position:
        x: '2706.73'
        'y': '38.92'
        z: '1159.23'
    - name: Coalition Airbase
      id: Coalition_Airbase
      position:
        x: '864.06'
        'y': '51.48'
        z: '3461.93'
---

