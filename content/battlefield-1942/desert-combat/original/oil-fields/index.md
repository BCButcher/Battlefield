---
title: Oil Fields
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Coalition Hq
      id: coalition_hq
      position:
        x: '304.44'
        'y': '17.09'
        z: '1218.66'
    - name: Refinerycomplex
      id: refinerycomplex
      position:
        x: '1500.00'
        'y': '17.00'
        z: '660.00'
    - name: Ainzalah
      id: ainzalah
      position:
        x: '1184.80'
        'y': '12.21'
        z: '1048.40'
    - name: Checkpoint
      id: checkpoint
      position:
        x: '882.22'
        'y': '9.57'
        z: '874.13'
    - name: Northernoilfields
      id: northernoilfields
      position:
        x: '1409.67'
        'y': '8.40'
        z: '1513.17'
    - name: South Check Point
      id: SouthCheckPoint
      position:
        x: '866.30'
        'y': '10.79'
        z: '422.14'
    - name: Village
      id: Village
      position:
        x: '731.14'
        'y': '12.75'
        z: '1246.69'
    - name: North Check Point
      id: NorthCheckPoint
      position:
        x: '1111.99'
        'y': '11.87'
        z: '1747.25'
---

