---
title: Sea Rigs
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Oil Rig 1 Topside
      id: OilRig1_Topside
      position:
        x: '984'
        'y': '134'
        z: '1058'
    - name: Oil Rig 2 Topside
      id: OilRig2_Topside
      position:
        x: '1042.166'
        'y': '133.9688'
        z: '1307.522'
    - name: Oil Spill
      id: Oil_Spill
      position:
        x: '900'
        'y': '80'
        z: '1182'
    - name: Oil Rig 1 Control Room
      id: OilRig1_Control_Room
      position:
        x: '984'
        'y': '115.7813'
        z: '1039'
    - name: Oil Rig 2 Control Room
      id: OilRig2_Control_Room
      position:
        x: '1042'
        'y': '115.78'
        z: '1325'
---

