---
title: Medina Ridge
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Oasis Town
      id: oasis_town
      position:
        x: '878.482'
        'y': '5.24375'
        z: '612.751'
    - name: Outpost Pass
      id: outpost_pass
      position:
        x: '428.869'
        'y': '22.4312'
        z: '894.285'
    - name: Opposition Base
      id: opposition_base
      position:
        x: '198.303'
        'y': '21.7125'
        z: '636.114'
    - name: Us Headquarters
      id: us_headquarters
      position:
        x: '800.329'
        'y': '5.275'
        z: '141.401'
---

