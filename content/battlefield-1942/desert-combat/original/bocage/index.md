---
title: Bocage
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Search and Destroy
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Search and Destroy
  size: 0
  source: original
  controlpoints: []
  created: '2004-02-04'
---

