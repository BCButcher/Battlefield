---
title: El Alamein Day2
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - Search and Destroy
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - Search and Destroy
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: South Open Base
      id: SouthOpenBase
      position:
        x: '905.766'
        'y': '86.3'
        z: '582.717'
    - name: North Open Base
      id: NorthOpenBase
      position:
        x: '874.005'
        'y': '51.5906'
        z: '1815.98'
    - name: East Open Base
      id: EastOpenBase
      position:
        x: '1343.9'
        'y': '45.3961'
        z: '1380.03'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '1694.08'
        'y': '60'
        z: '804.904'
    - name: Axis Base
      id: AxisBase
      position:
        x: '451.573'
        'y': '40.2'
        z: '1271.61'
---

