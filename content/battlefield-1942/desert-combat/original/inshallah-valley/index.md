---
title: Inshallah Valley
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Open Spawn Point Village
      id: OpenSpawnPoint_Village
      position:
        x: '557.1858'
        'y': '58.23173'
        z: '374.674'
    - name: Small Village
      id: Small_Village
      position:
        x: '524.6174'
        'y': '30.2716'
        z: '248.3743'
    - name: US Base
      id: US_Base
      position:
        x: '862.3945'
        'y': '97.56963'
        z: '600.0964'
    - name: Iraq Base
      id: Iraq_Base
      position:
        x: '303.7486'
        'y': '79.75867'
        z: '303.2622'
    - name: Outpost
      id: Outpost
      position:
        x: '434.0144'
        'y': '49.81406'
        z: '622.1573'
---

