---
title: Desert Combat
name: Desert Combat
description: >-
  Desert Combat (abbreviated DC) is a popular mod developed by Trauma Studios
  for Battlefield 1942. Set in Iraq during the First Gulf War, it added many new
  weapons, vehicles, and maps. After gaining popularity in the aftermath of the
  2003 invasion of Iraq.
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
origin: https://battlefield.fandom.com/wiki/Desert_Combat
banner: true
---

