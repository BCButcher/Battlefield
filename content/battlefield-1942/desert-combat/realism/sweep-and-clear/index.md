---
title: Sweep And Clear
author: ''
description: Move to the local buildings find any Enemies and remove them from the area
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: realism
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '512.00'
        'y': '257.29'
        z: '452.00'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '512.00'
        'y': '260.12'
        z: '572.00'
---

