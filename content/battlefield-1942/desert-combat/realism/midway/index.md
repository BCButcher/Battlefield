---
title: Midway
author: Trauma Studios, DC Final Team, DC Realism Team
description: ''
date: '2005-06-13'
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 0
  source: realism
  controlpoints:
    - name: The Airfield
      id: The_Airfield
      position:
        x: '2078.63'
        'y': '24.3187'
        z: '2112.66'
    - name: The Radar Bunker
      id: The_Radar_Bunker
      position:
        x: '1836.99'
        'y': '32.9391'
        z: '1972.83'
    - name: The Village
      id: The_Village
      position:
        x: '2147.41'
        'y': '24.3188'
        z: '2024.32'
  created: '2005-06-13'
  creator: Trauma Studios, DC Final Team, DC Realism Team
origin: >-
  https://web.archive.org/web/20220611145835/http://www.tanelorn.us/dcr/pages/maps.htm
---

