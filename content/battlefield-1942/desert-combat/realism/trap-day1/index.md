---
title: Trap Day1
author: ''
description: ' An unknown S.A.M. site recently took down a AC-130 on the outskirts of a small Iraq village. Secure the crash site and rescue any remaining survivers. '
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: realism
  controlpoints:
    - name: Axisbase
      id: axisbase
      position:
        x: '1582.91'
        'y': '126.79'
        z: '886.47'
    - name: Alliedbase
      id: alliedbase
      position:
        x: '410.23'
        'y': '154.11'
        z: '407.48'
    - name: Crashsitetemplate
      id: crashsitetemplate
      position:
        x: '1074.5'
        'y': '69.59'
        z: '1501.4'
---

