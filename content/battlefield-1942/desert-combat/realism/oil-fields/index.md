---
title: Oil Fields
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: realism
  controlpoints: []
---

