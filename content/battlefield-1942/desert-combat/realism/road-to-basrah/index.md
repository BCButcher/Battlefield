---
title: Road To Basrah
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: realism
  controlpoints:
    - name: Coalition Start Point
      id: Coalition_start_point
      position:
        x: '574.94'
        'y': '76.14'
        z: '1009.40'
    - name: Iraqi Strong Hold
      id: Iraqi_strong_hold
      position:
        x: '617.09'
        'y': '76.41'
        z: '28.82'
    - name: Road
      id: Road
      position:
        x: '486.29'
        'y': '76.20'
        z: '595.94'
    - name: East Village
      id: East_village
      position:
        x: '814.80'
        'y': '91.67'
        z: '495.16'
    - name: West Village
      id: West_village
      position:
        x: '259.91'
        'y': '91.74'
        z: '402.04'
    - name: Oposition House
      id: Oposition_house
      position:
        x: '389.62'
        'y': '76.81'
        z: '131.10'
    - name: Objective Two
      id: Objective_two
      position:
        x: '607.54'
        'y': '76.25'
        z: '198.50'
---

