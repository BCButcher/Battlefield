---
title: Battle for Anzio
author: DICE, EA
description: ''
date: '2003-02-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '1096.07'
        'y': '32.5711'
        z: '1156.19'
    - name: ALLIES BASE Harbur
      id: ALLIES_BASE_harbur
      position:
        x: '1296.65'
        'y': '29.4891'
        z: '1107.79'
    - name: Open Spawn Point Radiobunker
      id: OpenSpawnPoint_radiobunker
      position:
        x: '1301.2'
        'y': '41.3947'
        z: '1534.07'
    - name: Open Spawn Point O Bunker
      id: OpenSpawnPoint_o_bunker
      position:
        x: '1123.71'
        'y': '42.4992'
        z: '1387.86'
    - name: Axis BASE
      id: axis_BASE
      position:
        x: '952.457'
        'y': '32.4647'
        z: '1477.94'
    - name: Axis BASE Trainsation
      id: axis_BASE_trainsation
      position:
        x: '889.458'
        'y': '29.4908'
        z: '1677.49'
  created: '2003-02-02'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

