---
title: Operation Baytown
author: DICE, EA
description: ''
date: '2003-02-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Axis BASE Cpoint
      id: AxisBASE_Cpoint
      position:
        x: '1144.84'
        'y': '129.023'
        z: '717.106'
    - name: Axishousetop Cpoint
      id: Axishousetop_Cpoint
      position:
        x: '1210.31'
        'y': '109.597'
        z: '952.003'
    - name: Axisbeach Cpoint
      id: axisbeach_Cpoint
      position:
        x: '1078.01'
        'y': '55.0444'
        z: '948.112'
    - name: Openmidelpoint Cpoint
      id: openmidelpoint_Cpoint
      position:
        x: '1115.48'
        'y': '57.6891'
        z: '1109.11'
    - name: ALLIE Sbase Cpoint
      id: ALLIESbase_Cpoint
      position:
        x: '745.37'
        'y': '114.28'
        z: '1247.97'
    - name: ALLIE Shousonnext T Otop Cpoint
      id: ALLIEShousonnextTOtop_Cpoint
      position:
        x: '1208.35'
        'y': '88.6147'
        z: '1268.57'
    - name: ALLIE Sbeach Cpoint
      id: ALLIESbeach_Cpoint
      position:
        x: '1151.96'
        'y': '53.0656'
        z: '1218.62'
  created: '2003-02-02'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

