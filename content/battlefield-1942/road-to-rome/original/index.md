---
title: 'Battlefield 1942: Road To Rome'
name: Original
description: >-
  The intensive combats from Battlefield 1942 can now be experienced in new
  surroundings with olive trees and terracotta-colored houses. Road To Rome is
  an intensive experience played on six new maps around Italy.

  Choose between playing one of the previously available troops or perhaps one
  of the two new ones from the allied Free French and the Italian axis power.
  Eight new vehicles have been added, including twin-engine bombers and
  stationary anti-tank guns.
categories:
  - Battlefield 1942
  - Road To Rome
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20041001005948/http://global.dice.se/games/roadtorome/
---

