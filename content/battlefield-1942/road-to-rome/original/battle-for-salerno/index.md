---
title: Battle for Salerno
author: DICE, EA
description: ''
date: '2003-02-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '147.215'
        'y': '80.0227'
        z: '214.227'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '768.192'
        'y': '78.8607'
        z: '843.706'
    - name: The Top
      id: The_top
      position:
        x: '447.602'
        'y': '144.248'
        z: '492.623'
    - name: Allied Midpoint
      id: allied_midpoint
      position:
        x: '456.311'
        'y': '105.401'
        z: '718.916'
    - name: Axis Midpoint
      id: Axis_midpoint
      position:
        x: '493.322'
        'y': '122.03'
        z: '322.754'
  created: '2003-02-02'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

