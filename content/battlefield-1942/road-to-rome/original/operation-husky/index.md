---
title: Operation Husky
author: DICE, EA
description: ''
date: '2003-02-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 2048
  source: original
  controlpoints:
    - name: The Beach
      id: the_beach
      position:
        x: '1095.68'
        'y': '17.5556'
        z: '943.627'
    - name: The Beach Strong Point 1
      id: the_beach_strongPoint1
      position:
        x: '1012.88'
        'y': '25.4844'
        z: '1007.77 '
    - name: The Beach Strong Point 2
      id: the_beach_strongPoint2
      position:
        x: '896.453'
        'y': '35.7541'
        z: '1104.1'
    - name: The Beach Strong Pointtop
      id: the_beach_strongPointtop
      position:
        x: '998.936'
        'y': '56.3281'
        z: '1262.66'
    - name: The City Etry Bas 1
      id: the_City_etry_bas1
      position:
        x: '1135.35'
        'y': '41.9299'
        z: '1379.05'
    - name: The City Axisbase
      id: the_City_axisbase
      position:
        x: '1387.79'
        'y': '41.9333'
        z: '1444.25'
  created: '2003-02-02'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

