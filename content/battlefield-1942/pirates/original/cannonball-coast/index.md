---
title: Cannonball Coast
author: ''
description: >-
  Ahh, the white sand, the tropical breeze, the...Wait! is that a cannonball I
  see headed my way? I wonder if it's friendly...
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Skull Clan Camp
      id: SkullClan_Camp
      position:
        x: '1582.46'
        'y': '47.44'
        z: '247.95'
    - name: Peg Leg Harbor
      id: PegLeg_Harbor
      position:
        x: '258.76'
        'y': '45.39'
        z: '1642.43'
    - name: Chain Shot Point
      id: Chain_Shot_Point
      position:
        x: '612.45'
        'y': '90.45'
        z: '1237.14'
    - name: Powdermonkey Bluff
      id: Powdermonkey_Bluff
      position:
        x: '1239.35'
        'y': '71.40'
        z: '547.25'
    - name: Governors Castle
      id: Governors_Castle
      position:
        x: '962.00'
        'y': '69.00'
        z: '832.64'
  created: '2004-07-13'
---

