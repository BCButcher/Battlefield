---
title: Scurvy Cove Classic
author: ''
description: Pirates of a past age rise once more! Arrrr...
date: '2005-11-22'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: original
  controlpoints:
    - name: Blue Base Cpoint 2
      id: BlueBase_Cpoint2
      position:
        x: '633'
        'y': '66.463'
        z: '817.408'
    - name: Red Base Cpoint 2
      id: RedBase_Cpoint2
      position:
        x: '1411.707'
        'y': '58'
        z: '1189.037'
  created: '2005-11-22'
---

