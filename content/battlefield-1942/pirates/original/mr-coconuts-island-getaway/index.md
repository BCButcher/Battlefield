---
title: Mr Coconuts Island Getaway
author: ''
description: >-
  The PegLegs are taking a well-deserved vacation with their good friend Mr.
  Coconut. The SkullClan has just discovered that they weren't invited and have
  decided to crash the party. After a long swim from their ship, they've finally
  arrived.........There is only one kit for each side, so it doesn't matter
  which class you select. PegLegs start out on the island with only their frosty
  mug 'O ale, while the SkullClan is slightly better prepared, but has to swim
  ashore. If the PegLegs want better weapons, they're gonna have to steel them
  from their foes.
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Hill Top
      id: HillTop
      position:
        x: '524.86'
        'y': '55.91'
        z: '511.98'
    - name: Club Smiley
      id: Club_Smiley
      position:
        x: '577.78'
        'y': '40.22'
        z: '513.77'
    - name: Dangerous Waters
      id: Dangerous_Waters
      position:
        x: '531.50'
        'y': '-0.06'
        z: '264.46'
  created: '2004-07-13'
---

