---
title: Uncharted Waters
author: ''
description: Capture the control points to bleed the enemy.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Pegleg Camp
      id: Pegleg_Camp
      position:
        x: '722.65'
        'y': '88.41'
        z: '701.99'
    - name: Skull Clan Camp
      id: SkullClan_Camp
      position:
        x: '303.15'
        'y': '92.28'
        z: '341.57'
    - name: Skullclan Rim
      id: Skullclan_Rim
      position:
        x: '175.54'
        'y': '80.78'
        z: '846.51'
    - name: Pegleg Rim
      id: Pegleg_rim
      position:
        x: '840.42'
        'y': '80.61'
        z: '171.99'
  created: '2005-11-01'
---

