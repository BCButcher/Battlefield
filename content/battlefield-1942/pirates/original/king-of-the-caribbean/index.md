---
title: King Of The Caribbean
author: ''
description: >-
  The meanest scoundrels of the sea gather here to prove who is the King of the
  Caribbean! Take care of your Galleons because once they are gone, you might
  not get another..........Both sides start out with one galleon. If you can
  capture the center flag, a second will spawn at the top of the map. Hold the
  flags and destroy the enemy's ships to win the game. (And no, the double-flags
  in the center are not a mistake. they are necessary to make the spawn work
  right!)
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Island
      id: Island
      position:
        x: '1041.72'
        'y': '97.28'
        z: '1002.78'
    - name: East Landing
      id: East_Landing
      position:
        x: '1146.46'
        'y': '75.77'
        z: '1020.64'
    - name: West Landing
      id: West_Landing
      position:
        x: '917.36'
        'y': '76.19'
        z: '1025.62'
    - name: Island 2
      id: Island2
      position:
        x: '1041.74'
        'y': '97.33'
        z: '1002.26'
  created: '2004-07-13'
---

