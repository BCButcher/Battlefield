---
title: Iron Seas
author: ''
description: ''
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
  - TDM
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Skull Clan Hideout
      id: SkullClan_Hideout
      position:
        x: '1006.54'
        'y': '41.51'
        z: '1291.45'
    - name: Peg Leg Camp
      id: PegLeg_Camp
      position:
        x: '1003.36'
        'y': '41.47'
        z: '777.34'
    - name: Bullion Bay
      id: Bullion_Bay
      position:
        x: '1222.65'
        'y': '75.89'
        z: '1045.53'
    - name: Smugglers Cove
      id: Smugglers_Cove
      position:
        x: '838.79'
        'y': '41.03'
        z: '1037.90'
    - name: Beach
      id: Beach
      position:
        x: '1086.46'
        'y': '41.55'
        z: '976.60'
  created: '2004-07-13'
---

