---
title: Tides Of Blood
author: ''
description: ''
date: '2004-02-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Alpha 0.23
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: S Island
      id: S_Island
      position:
        x: '976.33'
        'y': '74.50'
        z: '660.59'
    - name: W Island
      id: W_Island
      position:
        x: '660.01'
        'y': '86.16'
        z: '988.68'
    - name: N Island
      id: N_Island
      position:
        x: '1235.85'
        'y': '65.12'
        z: '1652.19'
    - name: E Island
      id: E_Island
      position:
        x: '1663.00'
        'y': '60.36'
        z: '1245.06'
    - name: Center Island
      id: Center_Island
      position:
        x: '1157.25'
        'y': '76.76'
        z: '1147.61'
    - name: NW Island
      id: NW_Island
      position:
        x: '706.32'
        'y': '84.82'
        z: '1489.95'
    - name: SE Island
      id: SE_Island
      position:
        x: '1516.09'
        'y': '78.85'
        z: '691.82'
  created: '2004-02-11'
---

