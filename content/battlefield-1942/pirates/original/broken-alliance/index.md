---
title: Broken Alliance
author: ''
description: >-
  The Skullclan and Pelegs joined together to crack a tough fortress. After the
  town's surrender, both teams tunnel under the walls to cheat their pirate
  allies of the best booty! What are they to do when they discover each other's
  tunnels??? That's right! Scrap the alliance and scrap for the treasure!
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '492.63'
        'y': '92.36'
        z: '379.56'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '577.74'
        'y': '92.50'
        z: '672.66'
    - name: Ooga Booga Point
      id: Ooga_Booga_Point
      position:
        x: '206.06'
        'y': '82.91'
        z: '574.39'
    - name: Town Square
      id: Town_Square
      position:
        x: '556.50'
        'y': '83.99'
        z: '533.28'
    - name: Keep
      id: Keep
      position:
        x: '445.70'
        'y': '111.02'
        z: '536.40'
    - name: Treasury
      id: Treasury
      position:
        x: '420.28'
        'y': '108.08'
        z: '536.15'
  created: '2005-11-01'
---

