---
title: Dire Straits
author: ''
description: >-
  Rumor amongst the isles is that the King's treasure fleet will be passing
  through this strait in the morning. Whichever crew dominates the strait will
  get a shot at some prime booty. Note that the upper rigging of both galleons
  is closed for pre-battle upkeep.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '587.05'
        'y': '95.92'
        z: '81.38'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '445.31'
        'y': '95.60'
        z: '924.09'
  created: '2005-11-01'
---

