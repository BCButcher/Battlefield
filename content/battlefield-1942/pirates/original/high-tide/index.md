---
title: High Tide
author: ''
description: ''
date: '2004-02-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Alpha 0.23
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: original
  controlpoints:
    - name: Pirate Petes Hidey Hole
      id: Pirate_Petes_HideyHole
      position:
        x: '2064.28'
        'y': '50.34'
        z: '1668.27'
    - name: Dead Mans Shoal
      id: Dead_Mans_Shoal
      position:
        x: '2066.91'
        'y': '50.25'
        z: '2394.83'
    - name: Sixpence Sandbar
      id: Sixpence_Sandbar
      position:
        x: '2046.51'
        'y': '48.38'
        z: '1998.05'
    - name: Parrot Point
      id: Parrot_Point
      position:
        x: '2146.07'
        'y': '39.99'
        z: '2070.45'
    - name: Pelican Cliffs
      id: Pelican_Cliffs
      position:
        x: '1997.23'
        'y': '42.18'
        z: '2106.95'
  created: '2004-02-11'
---

