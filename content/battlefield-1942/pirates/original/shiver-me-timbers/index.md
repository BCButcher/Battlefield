---
title: Shiver Me Timbers
author: ''
description: ''
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Southern Island
      id: Southern_Island
      position:
        x: '559.39'
        'y': '111.50'
        z: '459.36'
    - name: Western Island
      id: Western_Island
      position:
        x: '427.89'
        'y': '111.50'
        z: '586.29'
    - name: Bridge Tower
      id: Bridge_Tower
      position:
        x: '463.21'
        'y': '113.00'
        z: '462.90'
    - name: Eastern Island
      id: Eastern_Island
      position:
        x: '647.92'
        'y': '106.00'
        z: '608.35'
    - name: Red Ctf
      id: Red_Ctf
      position:
        x: '427.65'
        'y': '95.46'
        z: '586.23'
    - name: Blue Ctf
      id: Blue_Ctf
      position:
        x: '559.31'
        'y': '95.74'
        z: '458.92'
  created: '2004-07-13'
---

