---
title: Blackbeards Booty
author: ''
description: >-
  You've finally found it: BlackBeard's Booty. But unfortunately, so have your
  rivals. The fight is on. Who can last long enough to claim the riches and who
  will be just bones drying on the beach?..........Capturing the center flag
  will cause enemy ticket bleed. However, this only lasts as long as you can
  keep a man alive near it, so guard it well! Outer flags are for strategic
  purposes, but stay captured even when you are not near them.
date: '2004-02-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Alpha 0.23
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Red Camp
      id: Red_Camp
      position:
        x: '447.04'
        'y': '48.10'
        z: '897.11'
    - name: Blue Camp
      id: Blue_Camp
      position:
        x: '528.00'
        'y': '49.36'
        z: '172.40'
    - name: South Shack
      id: South_Shack
      position:
        x: '372.25'
        'y': '47.70'
        z: '362.92'
    - name: North Shack
      id: North_Shack
      position:
        x: '350.83'
        'y': '50.78'
        z: '641.52'
    - name: East Clearing
      id: East_clearing
      position:
        x: '728.97'
        'y': '58.68'
        z: '503.03'
    - name: Black Beards Fort
      id: BlackBeards_Fort
      position:
        x: '504.71'
        'y': '53.03'
        z: '508.16'
  created: '2004-02-11'
---

