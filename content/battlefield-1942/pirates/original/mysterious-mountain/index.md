---
title: Mysterious Mountain
author: ''
description: >-
  Mysterious Mountain is a cursed isle, discovered in 1866. During this time,
  the height of the Skull Clan's power was strongest here but they left it
  deserted. After a year later, the Peglegs would come across it and they decide
  to claim the isle for themselves. Unfortunately, some unexpected visitors
  arrive to pick up some very vital maps located on the mountain.
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Fort
      id: Fort
      position:
        x: '429.68'
        'y': '92.58'
        z: '458.26'
    - name: Village
      id: Village
      position:
        x: '659.35'
        'y': '116.74'
        z: '675.91'
    - name: Mountain
      id: Mountain
      position:
        x: '533.46'
        'y': '129.50'
        z: '547.70'
    - name: NW Island
      id: NW_Island
      position:
        x: '372.62'
        'y': '76.85'
        z: '653.79'
    - name: SE Island
      id: SE_Island
      position:
        x: '658.64'
        'y': '76.40'
        z: '361.10'
    - name: Bunnys Grave
      id: BunnysGrave
      position:
        x: '451.84'
        'y': '87.31'
        z: '656.63'
    - name: Cove
      id: Cove
      position:
        x: '616.74'
        'y': '75.23'
        z: '426.80'
  created: '2004-07-13'
---

