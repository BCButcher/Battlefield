---
title: Rum Runner Temples
author: Archimonde
description: ''
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
    - TDM
  size: 1024
  source: anniversary
  controlpoints:
    - name: Temple Of Archi Monde
      id: Temple_Of_ArchiMonde
      position:
        x: '552.02'
        'y': '90.29'
        z: '482.59'
    - name: Axis Base
      id: AxisBase
      position:
        x: '654.80'
        'y': '112.59'
        z: '522.96'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '474.67'
        'y': '112.95'
        z: '433.15'
  created: '2007-09-21'
  creator: Archimonde
---

