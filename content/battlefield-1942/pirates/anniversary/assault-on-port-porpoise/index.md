---
title: Assault On Port Porpoise
author: Muad'Dib
description: >-
  After heavy battles the Putrid Porpoise is being repaired at Port Porpoise.
  The skullclan seize their opportunity to attack the port at night.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: New Control Point 1
      id: New_Control_Point1
      position:
        x: '212.67'
        'y': '127.74'
        z: '136.89'
    - name: Garrison
      id: Garrison
      position:
        x: '391.32'
        'y': '75.00'
        z: '889.67'
    - name: Church Square
      id: Church_square
      position:
        x: '315.47'
        'y': '89.30'
        z: '643.77'
    - name: Suburbs
      id: suburbs
      position:
        x: '171.49'
        'y': '89.31'
        z: '676.09'
    - name: City Square
      id: City_square
      position:
        x: '277.25'
        'y': '89.30'
        z: '721.96'
  created: '2007-09-21'
  creator: Muad'Dib
---

