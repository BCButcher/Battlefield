---
title: The Crossing
author: Cap'n Pugwash
description: Hold two of the middle flags if ye are to win this battle!
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Skullclan Base
      id: Skullclan_Base
      position:
        x: '516.80'
        'y': '114.39'
        z: '411.52'
    - name: Pegleg Base
      id: Pegleg_Base
      position:
        x: '516.56'
        'y': '114.39'
        z: '638.89'
  created: '2007-09-21'
  creator: Cap'n Pugwash
---

