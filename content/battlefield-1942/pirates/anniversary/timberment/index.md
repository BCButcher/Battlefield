---
title: Timberment
author: Stealth
description: >-
  The Skullclan wants something that lies in the humble fort of Timberment. The
  Peglegs intercepted the Skullclan after learning about their desire from an
  anonymous source. Both shall battle to the death over something that could be
  a complete myth. What lies in the fort of Timberment?
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '431.52'
        'y': '95.59'
        z: '528.66'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '619.60'
        'y': '98.08'
        z: '529.18'
  created: '2007-09-21'
  creator: Stealth
---

