---
title: Visby Ruins
author: Cap'n Pugwash
description: >-
  In the heart of the forest lies Visby Ruins, a much sought after prize for the
  pirates who have come to fight for it... Hold it at any cost!
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Skull Clan Town
      id: SkullClan_Town
      position:
        x: '759.78'
        'y': '76.22'
        z: '401.77'
    - name: Pegleg Town
      id: Pegleg_Town
      position:
        x: '531.90'
        'y': '76.22'
        z: '634.82'
  created: '2007-09-21'
  creator: Cap'n Pugwash
---

