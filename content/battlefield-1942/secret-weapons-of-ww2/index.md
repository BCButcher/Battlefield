---
title: Secret Weapons Of WWII
name: Secret Weapons Of Ww2
description: >-
  During the Second World War, both sides tried to find weapons that would end
  the war once and for all. Their determined research for a "secret weapon"
  resulted in many bizarre and almost surrealistic vehicles and weapons.
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
origin: >-
  https://web.archive.org/web/20041001010626/http://global.dice.se/games/secretweaponsofww2/
banner: true
---

