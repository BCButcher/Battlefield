---
title: Raid On Agheila
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Allies Base
      id: Allies_Base
      position:
        x: '1089.87'
        'y': '13.1613'
        z: '607.036'
    - name: Axis Base
      id: Axis_Base
      position:
        x: '819.588'
        'y': '32.908'
        z: '615.426'
    - name: South Base
      id: South_Base
      position:
        x: '1116.59'
        'y': '23.43'
        z: '1485.9'
    - name: West Base
      id: West_Base
      position:
        x: '734.492'
        'y': '43.1578'
        z: '1131.52'
    - name: East Base
      id: East_Base
      position:
        x: '1273.28'
        'y': '43.1578'
        z: '1170.18'
    - name: North Base
      id: North_Base
      position:
        x: '915.878'
        'y': '22.07'
        z: '1493.76'
    - name: Centre Base
      id: Centre_Base
      position:
        x: '1012.86'
        'y': '50.3625'
        z: '1120.48'
---

