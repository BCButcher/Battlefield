---
title: Kbely Airfield
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - Objectives
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - Objectives
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Axis Base
      id: axis_base
      position:
        x: '1149.51'
        'y': '43.4'
        z: '1232.81'
    - name: Allies Town
      id: allies_town
      position:
        x: '904.987'
        'y': '40.6138'
        z: '827.984'
    - name: Axis Town
      id: axis_town
      position:
        x: '1178.55'
        'y': '36.4793'
        z: '880.972'
    - name: Axis Town Two
      id: axis_town_two
      position:
        x: '1340.33'
        'y': '39.4788'
        z: '953.346'
    - name: Allies Base
      id: allies_base
      position:
        x: '882.168'
        'y': '44.2581'
        z: '633.336'
---

