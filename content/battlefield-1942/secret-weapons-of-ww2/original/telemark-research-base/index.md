---
title: Telemark Research Base
author: DICE, EA
description: ''
date: '2003-09-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - Objectives
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - Objectives
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '795.536'
        'y': '37.2133'
        z: '1444.98'
    - name: AXIS BASE Harbour
      id: AXIS_BASE_harbour
      position:
        x: '693.597'
        'y': '41.55'
        z: '835.069'
    - name: AXIS Bridge West
      id: AXIS_bridge_west
      position:
        x: '697.253'
        'y': '26.1969'
        z: '552.561'
    - name: Open Spawn Point Island
      id: OpenSpawnPoint_Island
      position:
        x: '1053.6'
        'y': '15.6504'
        z: '906.369'
    - name: Open Spawn Point O Bunker
      id: OpenSpawnPoint_o_bunker
      position:
        x: '1123.71'
        'y': '42.4992'
        z: '1387.86'
    - name: Axis BASE 1
      id: axis_BASE_1
      position:
        x: '1169.59'
        'y': '41.1261'
        z: '586.947'
    - name: Axis BASE 2
      id: axis_BASE_2
      position:
        x: '1268.75'
        'y': '35.5999'
        z: '837.048'
    - name: Axis BASE 3
      id: axis_BASE_3
      position:
        x: '982.188'
        'y': '14.6276'
        z: '921.071'
    - name: Axis BASE Bridge
      id: axis_BASE_bridge
      position:
        x: '845.006'
        'y': '26.1969'
        z: '528.801'
  created: '2003-09-04'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

