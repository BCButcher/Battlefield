---
title: Gothic Line
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Axis Airstrip
      id: axis_airstrip
      position:
        x: '867.226'
        'y': '87.4986'
        z: '457.35'
    - name: Alliedspawn
      id: alliedspawn
      position:
        x: '616.422'
        'y': '8.26602'
        z: '178.226'
    - name: Gothicline 1
      id: gothicline_1
      position:
        x: '732.117'
        'y': '38.3288'
        z: '436.05'
    - name: Gothicline 2
      id: gothicline_2
      position:
        x: '461.16'
        'y': '45.1631'
        z: '411.461'
    - name: Gothicline 3
      id: gothicline_3
      position:
        x: '494.867'
        'y': '61.3129'
        z: '505.165'
    - name: Neutral Airstrip
      id: neutral_airstrip
      position:
        x: '1160.83'
        'y': '87.15'
        z: '1303.92'
---

