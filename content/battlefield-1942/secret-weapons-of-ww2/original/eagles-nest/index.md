---
title: Eagles Nest
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - Objectives
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - Objectives
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: ALLIESBASE
      id: ALLIESBASE
      position:
        x: '825.159'
        'y': '42.8234'
        z: '415.302'
    - name: AXIS Bridge
      id: AXIS_bridge
      position:
        x: '635.784'
        'y': '37.7761'
        z: '550.843'
    - name: AXIS Compound
      id: AXIS_compound
      position:
        x: '405.112'
        'y': '61.9688'
        z: '449.124'
    - name: AXIS Goerings House
      id: AXIS_GoeringsHouse
      position:
        x: '262.383'
        'y': '78.6023'
        z: '434.146'
    - name: AXIS Tunnel
      id: AXIS_Tunnel
      position:
        x: '342.737'
        'y': '124.27'
        z: '750.011'
    - name: AXIS Eagles Nest
      id: AXIS_EaglesNest
      position:
        x: '182.288'
        'y': '153.797'
        z: '623.262'
---

