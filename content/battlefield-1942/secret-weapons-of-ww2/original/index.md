---
title: 'Battlefield 1942: Secret Weapons of WWII'
name: Original
description: >-
  Battlefield 1942: Secret Weapons of WW2 is the second expansion to Battlefield
  1942, which was elected Game Of The Year 2002, and lets you experience these
  strange and exciting weapons.

  Experience the secret military installations such as Telemark's heavy water
  factory, or the notorious airstrip at Kbely. Combat like a German elite
  soldier or as a British commando soldier. Fly legendary airplanes such as the
  Natter and the world's first stealth plane, the Horten Ho229.

  The game contains two new armies, eight new tracks, seven new weapons, and
  sixteen new vehicles.
categories:
  - Battlefield 1942
  - Secret Weapons of WWII
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20041001010626/http://global.dice.se/games/secretweaponsofww2/
---

