---
title: Mimoyecques
author: DICE, EA
description: ''
date: '2003-09-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - Objectives
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - Objectives
  size: 2048
  source: original
  controlpoints:
    - name: V 3 Complex
      id: 'V3_Complex '
      position:
        x: '984.151'
        'y': '81.0957'
        z: '442.68'
    - name: Allies Base
      id: 'AlliesBase '
      position:
        x: '973.203'
        'y': '25.7518'
        z: '1634.5'
    - name: Axis Beach
      id: 'axis_beach '
      position:
        x: '850.5'
        'y': '18.5367'
        z: '897.684'
    - name: Calais Neutral
      id: 'calais_neutral '
      position:
        x: '1071.47'
        'y': '20.0719'
        z: '906.173'
    - name: Axis Airport
      id: 'axis_airport '
      position:
        x: '911.395'
        'y': '55.5079'
        z: '525.759'
    - name: Mimoyecques
      id: 'mimoyecques '
      position:
        x: '951.5'
        'y': '26.1398'
        z: '793'
  created: '2003-09-04'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

