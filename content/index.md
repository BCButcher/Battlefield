---
title: Battlefield 42 & 2
layout: layouts/home.njk
games:
  - title: Battlefield 1942
    url: "/battlefield-1942"
    image: "./battlefield-1942/banner.jpg"
  - title: Battlefield 2
    url: "/battlefield-2"
    image: "./battlefield-2/banner.jpg"
---

Information, documentation, and maps for different mods made for Battlefield 1942 and 2. All content is ephemeral, and based on the maps included in the mods and metadata gathered across the web.
