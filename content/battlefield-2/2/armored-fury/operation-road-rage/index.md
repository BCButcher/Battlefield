---
title: Operation Road Rage
author: DICE, EA
description: >-
  The MEC forces have made landfall on the Eastern Coast of the United States
  and are preparing to push inland. Caught by surprise, the US Marines are
  deploying nearby, hastily preparing a base of operations to stop the MEC
  advance. The key objective for both armies is a highway junction in the middle
  of the battlefield that grants access to nearly every key military target in
  the area. Whoever controls this overpass controls most of the Eastern
  Seaboard!
date: '2005-06-21'
categories:
  - Battlefield 2
  - Armored Fury
tags:
  - Conquest
labels:
  - Armored Fury
map:
  gametypes:
    - Conquest
  sizes: []
  source: armored-fury
  controlpoints: []
  created: '2005-06-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Operation_Road_Rage
---

