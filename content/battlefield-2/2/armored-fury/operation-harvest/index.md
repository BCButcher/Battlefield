---
title: Operation Harvest
author: DICE, EA
description: >-
  Units of the MEC Second Armored have fought their way from a beachhead landing
  in the Delaware Bay to here in the Pennsylvania Dutch farmland of Lancaster
  County. This bold push is to cut off American units moving south to reinforce
  Washington D.C., a city under siege by MEC forces. This agriculturally rich
  area of American culture is about to erupt, as battle hardended units of
  America's Armored and Cavalry Divisions muster to stop the MEC Second Armored
  advance head on.
date: '2005-06-21'
categories:
  - Battlefield 2
  - Armored Fury
tags:
  - Conquest
labels:
  - Armored Fury
map:
  gametypes:
    - Conquest
  sizes: []
  source: armored-fury
  controlpoints: []
  created: '2005-06-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Operation_Harvest
---

