---
title: Midnight Sun
author: ''
description: ''
date: ''
categories:
  - Battlefield 2
  - Armored Fury
tags:
  - Conquest
labels: []
map:
  gametypes:
    - Conquest
  sizes: []
  source: armored-fury
  controlpoints: []
type: map
layout: layouts/map.njk
---

