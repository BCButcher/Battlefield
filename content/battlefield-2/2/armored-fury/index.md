---
title: 'Battlefield 2: Armored Fury'
name: Armored Fury
description: >-
  Battlefield 2: Armored Fury is the second booster pack released for
  Battlefield 2. It added three new maps, as well as two new vehicle classes:
  attack jets for close air support and reconnaissance helicopters that operate
  as a mobile UAV. The booster pack has the USMC defending U.S. soil from
  invasions from the PLA and MEC.
categories:
  - Battlefield 2
type: source
layout: layouts/source.njk
origin: https://en.wikipedia.org/wiki/Battlefield_2#Armored_Fury
banner: true
---

