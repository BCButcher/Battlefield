---
title: Battlefield 2
name: Battlefield 2
description: >-
  In Battlefield 2, players will choose to fight for one of three military
  superpowers: the United States, the Chinese, or the newly formed Middle East
  Coalition. Armed with the latest modern weaponry, players can take control of
  any of the game's 30+ vehicles to engage in major conflicts with over 64
  players in some of the largest online battles on the PC. Additionally,
  persistent character growth allows players to rise through the ranks and
  attain the ultimate rank of General.
categories:
  - Battlefield 2
type: game
layout: layouts/game.njk
origin: >-
  https://web.archive.org/web/20071010063403/http://battlefield.ea.com/battlefield/bf2/
banner: true
---

