---
title: Lost At Sea
author: Catbox
description: >-
  If ye fall overboard, there may be no hope for rescue in this stretch of
  sea... just remember that screaming will surely attract the sharks.
date: '2008-12-18'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: '2008-12-18'
  creator: Catbox
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20071012022220/http://bfpirates.com/phpBB2/viewtopic.php?t=1456
---

