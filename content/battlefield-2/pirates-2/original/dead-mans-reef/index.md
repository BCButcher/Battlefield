---
title: Dead Mans Reef
author: Rhino
description: >-
  After a string of rich plunders on the high seas, Capitan Rank was caught up
  in a fierce storm that ran his ship aground on a treacherous corral reef
  costing him and most of his crew there lives. According to one survivor, The
  plunder was lost in the wreckage and still lies somewhere in the reef in a
  locked chest, full of gold coins, preciouses stones and other treasurers.

  After hearing about Captain Rank's lost plunder the Peglegs raced to recover
  the booty. However, when they arrived they find that they are not the only
  ones looking for the rich takings!
date: '2009-02-03'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 2.5
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: '2009-02-03'
  creator: Rhino
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20231216142750/https://www.realitymod.com/forum/showthread.php?t=53350
---

