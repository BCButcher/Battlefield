---
title: Frylar 2
author: Catbox, Stealth?
description: >-
  Battlefield Pirates returns to the small island of Frylar. This small
  smuggling outpost was briefly abandoned due to the rising tides, until the
  undead claimed it for theirselves. Now the peglegs are back to fight for the
  left over booty.
date: '2008-12-18'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
  - Zombie
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - CTF
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: '2008-12-18'
  creator: Catbox, Stealth?
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20071012022220/http://bfpirates.com/phpBB2/viewtopic.php?t=1456
---

