---
title: Wake Island 1707
author: 1/2Hawk
description: >-
  While having Pirates way out in the Pacific is highly unlikely, its even less
  probable theyd be chasing around undead skeletons flying gliders made of rum
  kegs either.  YARR!  So we just hope you'll enjoy this map as much as we have.
date: '2008-12-18'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: '2008-12-18'
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20071012022220/http://bfpirates.com/phpBB2/viewtopic.php?t=1456
---

