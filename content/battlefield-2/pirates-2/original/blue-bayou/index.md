---
title: Blue Bayou
author: Pirate Plunder
description: >-
  Set deep in the Caribbean, the swampy river delta known only as Blue Bayou is
  home to the island of Lost Souls, one of the final resting places of the
  Undead. After a nasty storm, the Peglegs have found themselves shipwrecked on
  the northern island and cut off from the mainland, having only a nearby cave
  for shelter. The movement of treasure from the shipwreck to the cave has
  disturbed the Undead pirates, and they will soon be drawing down on the living
  and their booty. Can the Peglegs survive the upcoming assault in dense fog and
  tall reeds, where all noise is masked by the drone of crickets and bullfrogs
  singing at the moon?
date: '2007-09-20'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
  - Zombie
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - CTF
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: '2007-09-20'
  creator: Pirate Plunder
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20090611003637/http://bfpirates.com/wiki/index.php/Blue_Bayou
---

