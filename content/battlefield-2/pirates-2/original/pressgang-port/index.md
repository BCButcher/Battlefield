---
title: Pressgang Port
author: 1/2Hawk
description: >-
  This crossroads outpost in a port near Petit-Goave is a notorious haven for
  miscreants, drunkards, scoundrels, privateers and scallywags of all types. The
  fort was purposely built to keep the low life scum locked inside making it
  easier to pressgang them into naval service. In fact, its so difficult to
  leave that the dead still haunt their own half of town. Opposing factions have
  made it quite a sport to steal each others booty.
date: '2008-12-18'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: '2008-12-18'
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111400/http://bfpirates.com/wiki/index.php/Pressgang_Port
---

