---
title: Shiver Me Timbers
author: Catbox
description: >-
  Both the Peglegs and Undead see this chain of islands as the prime location
  for an new forward outpost- giving the victor naval superiority over the
  region.
date: '2007-09-20'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: '2007-09-20'
  creator: Catbox
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111329/http://bfpirates.com/wiki/index.php/BFP2_Shiver_Me_Timbers
---

