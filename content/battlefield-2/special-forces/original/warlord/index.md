---
title: Warlord
author: DICE, EA
description: >-
  An Insurgent Warlord has been commissioning attacks against British forces in
  the area with tremendous success, using local civilians as a shield against
  retribution. His reign of terror is about to be challenged. The SAS have been
  called in to execute a surgical strike on the Warlord's compound and
  surrounding Insurgent strongholds. Navigating perilous streets and alleys, the
  SAS must push towards the palace, but the Insurgents have the area
  well-guarded and won't give it up easily.
date: '2005-11-21'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels: []
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: '2005-11-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Warlord
---

