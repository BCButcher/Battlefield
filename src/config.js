module.exports = {
  indexFile: "index.md",
  layoutPrefix: "layouts/",
  layoutSuffix: ".njk",
};
