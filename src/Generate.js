const path = require("path"),
  FS = require("fs");
const {
  getArgs,
  makeFolder,
  log,
  info,
  warn,
  inspect,
} = require("./Utilities");
const Battlefield1942 = require("./generate/battlefield-1942");
const Battlefield2 = require("./generate/battlefield-2");
const args = getArgs();
const base = path.resolve("./").toLocaleLowerCase();
const dist = `${base}/content`;

if (args.debug && Object.keys(args).length > 0) {
  info({
    context: "DEBUG ARGS",
  });
  inspect(args);
}

makeFolder("./tmp");

if (args.hasOwnProperty("game") && args.game == "battlefield-1942") {
  Battlefield1942.process(args);
}
if (args.hasOwnProperty("game") && args.game == "battlefield-2") {
  Battlefield2.process(args);
}

setTimeout(function () {
  FS.rmSync("./tmp", { recursive: true });
  if (args.debug) warn({ context: "Nuked", messages: ["./tmp"] });
  log.shutdown();
}, 500);
