const child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  stripAnsi = require("strip-ansi"),
  stripBom = require("strip-bom"),
  collapse = require("collapse-white-space"),
  walker = require("klaw-sync"),
  TGA = require("tga"),
  PNG = require("pngjs").PNG,
  sharp = require("sharp"),
  { detector } = require("image-size/dist/detector"),
  humanize = require("humanize-string"),
  capitalize = require("capitalize"),
  lineByLine = require("n-readlines"),
  xml2js = require("xml2js"),
  fileExtension = require("file-extension"),
  YAML = require("js-yaml"),
  chalk = require("chalk"),
  typeCheck = require("type-check").typeCheck,
  log = require("./Logger");
const DDS = path.resolve("./bin/nconvert.exe");

class Utilities {
  /**
   * Group text files into a string
   * @param {string} dir Directory to search
   * @param {string} affix Glob-pattern
   * @returns {string|false}
   */
  static joinTextFiles({
    dir: dir,
    affix: affix = "",
    encoding: encoding = "utf8",
  }) {
    if (!FS.existsSync(dir) || !FS.lstatSync(dir).isDirectory()) {
      return false;
    }
    let pattern = dir;
    if (affix !== "") {
      pattern = `${dir}/${affix}`;
    }
    let files = walker(pattern, {
      fs: FS,
      depthLimit: 0,
      nodir: true,
    });
    if (files.length < 1) {
      return false;
    }
    let contents;
    for (let i = 0; i < files.length; i++) {
      let fileContent = FS.readFileSync(files[i].path, encoding) + "\n";
      if (encoding == "ucs2") {
        fileContent = fileContent.replaceAll("", "");
      }
      contents += fileContent;
    }
    return contents;
  }

  /**
   * Get content of line in string
   * @param {string} source String to search
   * @param {string} subString Line will include this
   * @returns {string|false} Sanitized content
   */
  static getLineContent({ source: source, subString: subString }) {
    if (source == null || subString == null) {
      return false;
    }
    let match = "";
    let lines = source.split(/\r?\n/);
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].includes(subString)) {
        match = lines[i].replace(subString, "").trim();
      }
    }
    return match;
  }

  /**
   * Get content of line from file
   * @param {string} file Path to file
   * @param {string} subString Line will include this
   * @param {object} regex Regular expression to match content
   * @returns {string|false} Sanitized content
   */
  static getFileLineContent({
    file: file,
    subString: subString,
    regex: regex = null,
  }) {
    if (file == null || subString == null) {
      return false;
    }
    const liner = new lineByLine(file);
    let line;
    let lineNumber = 0;
    let result = null;
    while ((line = liner.next())) {
      if (line.toString("ascii").startsWith(subString)) {
        result = line.toString("ascii").replace(subString, "");
        if (regex && result.match(regex) && result.match(regex)[0]) {
          result.match(regex)[0];
        }
      }
      lineNumber++;
    }
    if (result !== null) {
      result = result
        .replaceAll(/(^"|"$)/g, "")
        .replaceAll(/(\r\n|\n|\r)/gm, "")
        .replaceAll(/ +(?= )/g, "");
    }
    return result;
  }

  /**
   * Get content by regular expression from file
   * @param {string} file Path to file
   * @param {object} regex Regular expression to match content
   * @param {string} startsWith Prefix to strip
   * @returns {string} Sanitized content
   */
  static getRegexContent({
    file: file,
    regex: regex,
    startsWith: startsWith = "",
    encoding: encoding = "utf8",
    stripBOM: stripBOM = false,
  }) {
    if (file == null || regex == null) {
      return false;
    }
    if (!FS.existsSync(file)) {
      return false;
    }
    try {
      new RegExp(regex, "gmu");
    } catch (error) {
      log.error({
        context: "Utilities.getRegexContent() RegExp()",
        error: error,
      });
      return false;
    }
    let result = null;
    let content = FS.readFileSync(file, encoding);
    if (stripBOM) {
      content = stripBom(content);
    }
    if (
      regex !== null &&
      content.match(regex) !== null &&
      content.match(regex)[0] !== null
    ) {
      result = content.match(regex)[0];
    }
    if (startsWith && result !== null) {
      result = result.replace(startsWith, "");
    }
    if (result !== null) {
      result = result
        .replaceAll(/(^"|"$)/g, "")
        .replaceAll(/(\r\n|\n|\r)/gm, "  ")
        .replaceAll(/ +(?= )/g, "");
    }
    return result;
  }

  /**
   * Get content by regular expression from Lexicon-file
   * @param {string} files Paths to files to search, ordered by priority
   * @param {object} target Identifier to search for
   * @returns {string} Sanitized content
   */
  static getLexiconContent({ files: files, target: target }) {
    if (typeof target !== "string" || target == "") {
      return false;
    }
    const validatelLexiconLookup = /^[A-Z_]+$/gm;
    if (!validatelLexiconLookup.test(target)) {
      return false;
    }
    let content = "";
    let captureLexiconRegex = `${target}\\s*[#]{2}\\s*([\\S\\s]*?)\\s*[#]{2}`;
    try {
      new RegExp(captureLexiconRegex, "gmu");
    } catch (error) {
      log.error({
        context: "Utilities.getLexiconContent() RegExp()",
        error: error,
      });
      return false;
    }
    for (let i = 0; i < files.length; i++) {
      if (!FS.existsSync(files[i])) {
        continue;
      }
      let result = Utilities.getRegexContent({
        file: files[i],
        regex: new RegExp(captureLexiconRegex, "gmu"),
        startsWith: target,
        encoding: "utf16le",
        stripBOM: true,
      });
      if (typeof result === "string" && result.length > 0) {
        content = result;
      }
    }
    if (content) {
      content = collapse(content.replace(/[#]{2}/g, "").trim());
    }
    return content;
  }

  /**
   * Get content from XML in file
   * @param {string} filename Path to file
   * @returns {object} Parsed XML to JSON
   */
  static getXMLContent({ file: file }) {
    const parser = new xml2js.Parser();
    let content = FS.readFileSync(file, "utf8");
    let parsed;
    parser.parseString(content, function (error, result) {
      if (error) {
        log.error(error, input, "=>", output);
      }
      parsed = result;
    });
    return parsed;
  }

  /**
   * Write Markdown file
   * @param {string} file Path to file
   * @param {object} metadata FrontMatter
   * @param {string} type Type of content
   * @param {string} name Name of content
   */
  static writeMarkdown({
    file: file,
    metadata: metadata,
    content: content = "",
    type: type,
    name: name,
    base: base,
  }) {
    if (!FS.existsSync(path.dirname(file))) {
      FS.mkdirSync(path.dirname(file), { recursive: true });
    }
    if (!metadata.length > 1) {
      log.warn({
        context: "No metadata",
        messages: [Utilities.type(type), Utilities.item(name)],
      });
      return;
    }
    let data = "";

    try {
      data = YAML.dump(metadata);
    } catch (error) {
      log.error({
        context: "Utilities.writeMarkdown() YAML.dump()",
        messages: [Utilities.type(type), Utilities.item(name)],
        error: error,
      });
    }

    try {
      FS.writeFileSync(file, `---\n${data}---\n\n${content}`);
      log.create({
        source: Utilities.name(name),
        target: file.replace(base, ""),
        prefix: Utilities.name(type),
      });
    } catch (error) {
      log.error({
        context: "Utilities.writeMarkdown() FS.writeFileSync()",
        messages: [Utilities.type(type), Utilities.item(name)],
        error: error,
      });
    }
  }

  /**
   * Recursive, forceful folder-creation
   * @param {string} target Path to directory
   */
  static makeFolder(target) {
    if (!FS.existsSync(target)) {
      FS.mkdirSync(target, { recursive: true });
    }
  }

  /**
   * Convert image to JPG
   * @param {string} input Path to image-file to read
   * @param {string} output Path to JPG-file to write
   * @returns {boolean} Operation status
   */
  static image2jpg({ input: input = false, output: output = false }) {
    if (!input || !output) {
      return false;
    }
    let type = detector(FS.readFileSync(input));
    if (type == "tga") {
      return Utilities.tga2jpg({ input: input, output: output });
    } else if (type == "dds") {
      return Utilities.dds2jpg({ input: input, output: output });
    } else if (type == "png") {
      return Utilities.png2jpg({ input: input, output: output });
    }
    return false;
  }

  /**
   * Convert TGA to JPG
   * @param {string} input Path to TGA-file to read
   * @param {string} output Path to JPG-file to write
   * @returns {boolean} Operation status
   */
  static tga2jpg({ input: input = false, output: output = false }) {
    if (!input || !output) {
      return false;
    }
    let tga = new TGA(FS.readFileSync(input));
    let png = new PNG({
      width: tga.width,
      height: tga.height,
    });
    png.data = tga.pixels;
    let data = PNG.sync.write(png);
    if (data) {
      Utilities.png2jpg({ input: data, output: output });
    }
    return true;
  }

  /**
   * Convert DDS to JPG
   * @param {string} input Path to DDS-file to read
   * @param {string} output Path to JPG-file to write
   * @returns {boolean} Operation status
   */
  static dds2jpg({ input: input = false, output: output = false }) {
    if (!input || !output) {
      return false;
    }
    let tmpTarget = output
      .replace("/content/", "/src/tmp/")
      .replace(".jpg", ".png");
    if (!FS.existsSync(path.dirname(tmpTarget))) {
      FS.mkdirSync(path.dirname(tmpTarget), { recursive: true });
    }
    child_process
      .execSync(`${DDS} -overwrite -out png -o "${tmpTarget}" "${input}"`, {
        timeout: 10000,
        stdio: "pipe",
      })
      .toString();
    Utilities.png2jpg({ input: tmpTarget, output: output });
    return true;
  }

  /**
   * Convert PNG to JPG
   * @param {string} input Path to PNG-file to read
   * @param {string} output Path to JPG-file to write
   * @returns {boolean} Operation status
   */
  static png2jpg({ input: input = false, output: output = false }) {
    if (!input || !output) {
      return false;
    }
    sharp(input)
      .resize(1200, null, { withoutEnlargement: true })
      .jpeg({ mozjpeg: true })
      .toFile(output, function (error) {
        if (error) {
          log.error("png2jpg()", [input, "=>", output], error);
        }
      });
    return true;
  }

  /**
   * Format string for Type
   * @param {string} str
   * @returns Colored, formatted string
   */
  static type(str) {
    return chalk.cyanBright(Utilities.name(str));
  }

  /**
   * Format string for Item
   * @param {string} str
   * @returns Colored, formatted string
   */
  static item(str) {
    return chalk.whiteBright(Utilities.name(str));
  }

  /**
   * Clean and humanize name
   * @param {string} name
   * @returns string
   */
  static name(name) {
    return capitalize
      .words(humanize(Utilities.cleanPrefix(name.toLocaleLowerCase())))
      .trim();
  }

  /**
   * Clean prefixes from a name
   * @param {string} name
   * @returns string
   */
  static cleanPrefix(name) {
    return name
      .replace("bfp_", "")
      .replace("sct_", "")
      .replace("gc_", "")
      .replace("dc_", "")
      .replace("dcr_", "");
  }

  /**
   * Clean affixes from a name
   * @param {string} name
   * @returns string
   */
  static cleanAffix(name) {
    return name.replace("_ctf", "").replace("_zm", "");
  }

  /**
   * Clean prefixes from a image path
   * @param {string} name
   * @returns string
   */
  static cleanImagePath({ path: path, name: name = false }) {
    if (name) {
      path = path.replace(name, "");
    }
    return path.replace("../../bf1942/levels/", "");
  }

  /**
   * Generate a random, alpanumeric string
   * @param {int} len
   * @returns Random string of given length
   * @see https://stackoverflow.com/a/27872144
   */
  static randomString(len = 10) {
    var str = "";
    for (var i = 0; i < len; i++) {
      var rand = Math.floor(Math.random() * 62);
      var charCode = (rand += rand > 9 ? (rand < 36 ? 55 : 61) : 48);
      str += String.fromCharCode(charCode);
    }
    return str;
  }

  /**
   * Get process arguments
   * @returns object
   * @see https://stackoverflow.com/a/54098693
   */
  static getArgs() {
    const args = {};
    process.argv.slice(2, process.argv.length).forEach((arg) => {
      if (arg.slice(0, 2) === "--") {
        const longArg = arg.split("=");
        const longArgFlag = longArg[0].slice(2, longArg[0].length);
        const longArgValue = longArg.length > 1 ? longArg[1] : true;
        if (typeof longArgValue === "string") {
          args[longArgFlag] = longArgValue.toLowerCase();
        } else {
          args[longArgFlag] = longArgValue;
        }
      } else if (arg[0] === "-") {
        const flags = arg.slice(1, arg.length).split("");
        flags.forEach((flag) => {
          args[flag] = true;
        });
      }
    });
    return args;
  }

  /**
   * Require parameter
   * @param {string} name Parameter-name
   * @throws Throw an error if the supplied argument is missing
   */
  static param(name) {
    throw new Error(`Required parameter ${name} is missing.`);
  }

  /**
   * Test type
   * @param {string} type Input type
   * @param {string} name Input name
   * @throws Throw an error if the type is incorrect
   */
  static checkType(type, name) {
    throw new Error(`Required parameter ${name} is missing.`);
  }
}

module.exports = {
  joinTextFiles: Utilities.joinTextFiles,
  getLineContent: Utilities.getLineContent,
  getFileLineContent: Utilities.getFileLineContent,
  getRegexContent: Utilities.getRegexContent,
  getLexiconContent: Utilities.getLexiconContent,
  getXMLContent: Utilities.getXMLContent,
  writeMarkdown: Utilities.writeMarkdown,
  makeFolder: Utilities.makeFolder,
  image2jpg: Utilities.image2jpg,
  time: log.time,
  msToTime: log.msToTime,
  name: Utilities.name,
  cleanPrefix: Utilities.cleanPrefix,
  cleanAffix: Utilities.cleanAffix,
  cleanImagePath: Utilities.cleanImagePath,
  randomString: Utilities.randomString,
  getArgs: Utilities.getArgs,
  param: Utilities.param,
  log: log,
  info: log.info,
  warn: log.warn,
  error: log.error,
  inspect: log.inspect,
  iterate: log.iterate,
  chalk: chalk,
};
