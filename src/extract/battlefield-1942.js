const child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  replaceAll = require("string.prototype.replaceall"),
  fileExtension = require("file-extension"),
  { getArgs, name, log, info, warn, iterate, chalk } = require("../Utilities");
replaceAll.shim();
const start = performance.now();
const base = path.resolve("./").replaceAll("\\", "/").toLowerCase();
const dist = `${base}/extract`;
const args = getArgs();

const walkerOptions = {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    const levels = item.path
      .replaceAll("\\", "/")
      .toLowerCase()
      .replace(`${base}/games/`, "")
      .replace(`.${fileExtension(item.path)}`, "")
      .split("/");
    if (args.hasOwnProperty("mod") && levels[1] !== args.mod) {
      return false;
    }
    if (args.hasOwnProperty("source") && levels[2] !== args.source) {
      return false;
    }
    if (
      args.hasOwnProperty("map") &&
      typeof levels[3] !== "undefined" &&
      !levels[3].includes(args.map)
    ) {
      return false;
    }
    return fileExtension(item.path) === "rfa";
  },
};

class Battlefield1942 {
  static process({ debug: debug = false }) {
    const RFAUnpacker = `${base}/bin/rfaUnpack.exe`;
    const gameBase = `${base}/games/battlefield-1942`;
    const gameBasename = path.basename(gameBase);
    if (args.hasOwnProperty("game") && args.game !== gameBasename) {
      return;
    }
    const files = walker(`${base}/games/battlefield-1942`, walkerOptions);
    info({
      context: "Found",
      messages: [`${files.length} Map(s)`],
    });
    for (let i = 0; i < files.length; i++) {
      const extension = fileExtension(files[i].path);
      const basename = path.basename(files[i].path, `.${extension}`);
      let target = files[i].path
        .toLowerCase()
        .replaceAll("\\", "/")
        .replace(`${base}/games`, "")
        .replace(/^\/|\/$/g, "");
      const parentTarget = path
        .resolve(files[i].path, "..")
        .replaceAll("\\", "/")
        .replace(`${base}/games`, "")
        .toLowerCase();
      if (parentTarget !== "" && !FS.existsSync(`${dist}/${parentTarget}`)) {
        FS.mkdirSync(`${dist}/${parentTarget}`, { recursive: true });
      }
      iterate({
        i: i + 1,
        n: files.length,
        message: name(
          target.replace(`.${extension}`, "").replaceAll("/", " / "),
        ),
      });
      target = target.replace(`.${extension}`, "");
      let tmpTarget = `tmp/${target}`;
      if (!FS.existsSync(`${dist}/${tmpTarget}`)) {
        FS.mkdirSync(`${dist}/${tmpTarget}`, { recursive: true });
      }
      if (!FS.existsSync(`${dist}/${target}`)) {
        FS.mkdirSync(`${dist}/${target}`, { recursive: true });
      }
      child_process.execSync(
        `"${RFAUnpacker}" "${files[i].path}" "${dist}/${tmpTarget}"`,
        {
          timeout: 300000,
        },
      );
      if (debug)
        info({
          context: "Extracted",
          messages: [tmpTarget],
          time: start,
        });
      if (FS.existsSync(`${dist}/${tmpTarget}`)) {
        FS.rmSync(`${dist}/${target}`, { recursive: true, force: true });
        if (debug)
          warn({
            context: "Removed",
            messages: [tmpTarget],
          });
        FS.renameSync(
          `${dist}/${tmpTarget}/bf1942/levels/${basename}`,
          `${dist}/${target}`,
        );
        if (debug)
          log.create({
            source: tmpTarget,
            target: target,
            prefix: "Renamed",
          });
      }
      log.create({
        source: name(basename),
        target: target,
        prefix: "Map",
        time: start,
      });
    }
  }
}

module.exports = Battlefield1942;
