const { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  replaceAll = require("string.prototype.replaceall"),
  slugify = require("slugify"),
  fileExtension = require("file-extension"),
  config = require("../config"),
  chalk = require("chalk"),
  {
    getFileLineContent,
    getRegexContent,
    getLexiconContent,
    writeMarkdown,
    makeFolder,
    image2jpg,
    name,
    cleanPrefix,
    getArgs,
  } = require("../Utilities"),
  log = require("../Logger"),
  collapse = require("collapse-white-space"),
  { startCase, upperFirst, uniq } = require("lodash");
const Utilities = require("../Utilities");
replaceAll.shim();
const start = performance.now();
const base = path.resolve("./").replaceAll("\\", "/").toLocaleLowerCase();
const dist =
  path.resolve("../").replaceAll("\\", "/").toLocaleLowerCase() + `/content`;
const Derivatives = require("./../data/derivatives.json");
const args = getArgs();

const walkerOptions = {
  fs: FS,
  nofile: true,
  depthLimit: 0,
};

class Battlefield1942 {
  static process({ debug: debug = false }) {
    log.info({
      context: "TARGET",
      messages: [dist],
    });
    const gameBase = `${base}/extract/battlefield-1942`;
    const gameBasename = path.basename(gameBase);
    if (args.hasOwnProperty("game") && args.game !== gameBasename) {
      return;
    }
    let game = Battlefield1942.meta({
      type: "game",
      basename: gameBasename,
      source: `${base}/data/${gameBasename}`,
    });
    writeMarkdown({
      file: `${dist}/${gameBasename}/${config.indexFile}`,
      metadata: game,
      type: "game",
      name: gameBasename,
      base: dist,
    });
    let mods, sources, maps;
    mods = walker(gameBase, walkerOptions);
    for (let i = 0; i < mods.length; i++) {
      const modBasename = path.basename(mods[i].path);
      if (args.hasOwnProperty("mod") && args.mod !== modBasename) {
        continue;
      }
      let mod = Battlefield1942.meta({
        type: "mod",
        basename: modBasename,
        source: `${base}/data/${gameBasename}/${modBasename}`,
      });
      writeMarkdown({
        file: `${dist}/${gameBasename}/${modBasename}/${config.indexFile}`,
        metadata: mod,
        type: "mod",
        name: modBasename,
        base: dist,
      });
      sources = walker(
        `${base}/extract/${gameBasename}/${modBasename}`,
        walkerOptions,
      );
      for (let n = 0; n < sources.length; n++) {
        const sourceBasename = path.basename(sources[n].path);
        if (args.hasOwnProperty("source") && args.source !== sourceBasename) {
          continue;
        }
        let source = Battlefield1942.meta({
          type: "source",
          basename: sourceBasename,
          source: `${base}/data/${gameBasename}/${modBasename}/${sourceBasename}`,
        });
        writeMarkdown({
          file: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${config.indexFile}`,
          metadata: source,
          type: "source",
          name: sourceBasename,
          base: dist,
        });
        maps = walker(
          `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}`,
          walkerOptions,
        );
        for (let m = 0; m < maps.length; m++) {
          const mapBasename = path.basename(maps[m].path);
          if (args.hasOwnProperty("map") && !mapBasename.includes(args.map)) {
            continue;
          }
          let mapTarget = cleanPrefix(mapBasename).replaceAll("_", "-");
          let map = Battlefield1942.mapMetadata({
            file: mapTarget,
            self: `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
            source: sourceBasename,
            dataSource: `${base}/data/${gameBasename}/${modBasename}/${sourceBasename}`,
            debug: debug,
          });
          mapTarget = slugify(map.title, { lower: true, strict: true });
          makeFolder(
            `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}`,
          );
          if (
            source.name &&
            source.name.length > 0 &&
            !map.categories.includes(source.name)
          ) {
            map.categories.push(source.name);
          }
          Battlefield1942.images({
            source: `${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
            target: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}`,
            dataSource: `${base}/data/${gameBasename}/${modBasename}/${sourceBasename}`,
            debug: debug,
          });
          const derivative = Battlefield1942.derivative({
            gameBasename: gameBasename,
            modBasename: modBasename,
            sourceBasename: sourceBasename,
            mapBasename: mapBasename,
            debug: debug,
          });
          if (derivative) {
            map.original = derivative;
            map.labels.push("Modified");
          }

          let content = Battlefield1942.mapContent({
            name: mapBasename,
            game: `${base}/extract/${gameBasename}`,
            mod: `${base}/extract/${gameBasename}/${modBasename}`,
            source: `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}`,
            map: `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
            debug: debug,
          });

          writeMarkdown({
            file: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}/${config.indexFile}`,
            metadata: map,
            content: content,
            type: "map",
            name: mapBasename,
            base: dist,
          });
        }
      }
    }
  }

  static meta({
    type: type,
    basename: basename,
    source: source,
    debug: debug,
  }) {
    let metadata = {
      title: name(basename),
      name: name(basename),
      description: "",
      categories: ["Battlefield 1942"],
      type: type,
      layout: `${config.layoutPrefix}${type}${config.layoutSuffix}`,
    };
    if (FS.existsSync(`${source}/metadata.json`)) {
      if (debug)
        log.info({
          context: "Read",
          messages: [`${source.replace(base, "")}/metadata.json`],
        });
      const Metadata = require(`${source}/metadata.json`);
      if (Metadata.hasOwnProperty("title") && Metadata["title"] !== "") {
        metadata.title = Metadata["title"];
      }
      if (Metadata.hasOwnProperty("name") && Metadata["name"] !== "") {
        metadata.name = Metadata["name"];
      }
      if (
        Metadata.hasOwnProperty("description") &&
        Metadata["description"] !== ""
      ) {
        metadata.description = Metadata["description"];
      }
      if (
        Metadata.hasOwnProperty("categories") &&
        Metadata["categories"].length > 0
      ) {
        metadata.categories = metadata.categories.concat(
          Metadata["categories"],
        );
        metadata.categories = uniq(metadata.categories);
      }
      if (Metadata.hasOwnProperty("tags") && Metadata["tags"].length > 0) {
        metadata.tags = metadata.tags.concat(Metadata["tags"]);
        metadata.tags = uniq(metadata.tags);
      }
      if (Metadata.hasOwnProperty("origin") && Metadata["origin"].length > 0) {
        metadata.origin = Metadata["origin"];
      }
      if (
        Metadata.hasOwnProperty("derivatives") &&
        Metadata["derivatives"].length > 0
      ) {
        metadata.derivatives = Metadata["derivatives"];
      }
    }
    if (FS.existsSync(`${source}/banner.png`)) {
      image2jpg({
        input: `${source}/banner.png`,
        output: `${dist}/${source
          .replace(base, "")
          .replace("/data", "")}/banner.jpg`,
      });
      metadata.banner = true;
    }
    return metadata;
  }

  static mapContent({
    name: name,
    game: game,
    mod: mod,
    source: source,
    map: map,
    debug: debug,
  }) {
    let lexiconData = {
      briefing: "",
      campaign: {
        allied: "",
        axis: "",
      },
      objectives: {
        allied: "",
        axis: "",
      },
    };
    let targets = [
      `${source}/lexiconAll.txt`,
      `${mod}/lexiconAll.txt`,
      `${game}/1942/lexiconAll.txt`,
    ];
    name = name.toUpperCase();

    lexiconData.briefing = getLexiconContent({
      files: targets,
      target: getRegexContent({
        file: `${map}/menu/init.con`,
        startsWith: "game.setMultiplayerBriefingObjectives ",
        regex: /game\.setMultiplayerBriefingObjectives ([\S\s]*?)$/gm,
      }),
    });
    lexiconData.campaign.allied = getLexiconContent({
      files: targets,
      target: getRegexContent({
        file: `${map}/menu/init.con`,
        startsWith: "game.setAlliedCampaign ",
        regex: /game\.setAlliedCampaign ([\S\s]*?)$/gm,
      }),
    });
    lexiconData.campaign.axis = getLexiconContent({
      files: targets,
      target: getRegexContent({
        file: `${map}/menu/init.con`,
        startsWith: "game.setAxisCampaign ",
        regex: /game\.setAxisCampaign ([\S\s]*?)$/gm,
      }),
    });
    lexiconData.objectives.allied = getLexiconContent({
      files: targets,
      target: getRegexContent({
        file: `${map}/menu/init.con`,
        startsWith: "game.setAlliedObjectives ",
        regex: /game\.setAlliedObjectives ([\S\s]*?)$/gm,
      }),
    });
    lexiconData.objectives.axis = getLexiconContent({
      files: targets,
      target: getRegexContent({
        file: `${map}/menu/init.con`,
        startsWith: "game.setAxisObjectives ",
        regex: /game\.setAxisObjectives ([\S\s]*?)$/gm,
      }),
    });

    let content = "";
    if (lexiconData.briefing && lexiconData.briefing.length > 0) {
      content += `${lexiconData.briefing}\n\n`;
    }
    if (lexiconData.campaign.allied && lexiconData.campaign.allied.length > 0) {
      content += `## Allied Campaign\n\n${lexiconData.campaign.allied}\n\n`;
    }
    if (lexiconData.campaign.axis && lexiconData.campaign.axis.length > 0) {
      content += `## Axis Campaign\n\n${lexiconData.campaign.axis}\n\n`;
    }
    if (
      lexiconData.objectives.allied &&
      lexiconData.objectives.allied.length > 0
    ) {
      content += `## Allied Objectives\n\n${lexiconData.objectives.allied}\n\n`;
    }
    if (lexiconData.objectives.axis && lexiconData.objectives.axis.length > 0) {
      content += `## Axis Objectives\n\n${lexiconData.objectives.axis}\n\n`;
    }

    if (debug)
      log.info({
        context: "Built",
        messages: ["Content"],
        time: start,
        color: "blueBright",
      });
    return content;
  }

  static mapMetadata({
    file: file,
    self: self,
    source: source,
    dataSource: dataSource,
    debug: debug,
  }) {
    let metadata = {
      title: name(file),
      author: "",
      description: "",
      date: "",
      categories: ["Battlefield 1942"],
      tags: [],
      labels: [],
      type: "map",
      layout: `${config.layoutPrefix}map${config.layoutSuffix}`,
      content: "",
      map: {
        gametypes: [],
        size: 0,
        source: "",
        controlpoints: [],
      },
    };
    if (source !== null && source !== "") {
      metadata.categories.push(name(source));
      metadata.map.source = source;
    }

    /* Map description */
    if (FS.existsSync(`${self}/menu/init.con`)) {
      let description = getRegexContent({
        file: `${self}/menu/init.con`,
        startsWith: "game.setMultiplayerBriefingObjectives ",
        regex: /game\.setMultiplayerBriefingObjectives "([\S\s]*?)"/gm,
      });
      if (
        description &&
        !description.includes("put your level destription here")
      ) {
        metadata.description = description;
      }
    }

    /* Map gametypes */
    if (FS.existsSync(`${self}/gametypes`)) {
      let gametypes = FS.readdirSync(`${self}/gametypes`);
      for (let n = 0; n < gametypes.length; n++) {
        let ext = fileExtension(gametypes[n]);
        let type = path.basename(gametypes[n], `.${ext}`);
        if (type.toUpperCase() === "CTF" || type.toUpperCase() === "TDM") {
          type = type.toUpperCase();
        }
        if (type.toUpperCase() === "OBJECTIVEMODE") {
          type = "Objectives";
        }
        if (type.toUpperCase() === "SEARCH_AND_DESTROY") {
          type = "Search and Destroy";
        }
        metadata.map.gametypes.push(upperFirst(type));
        metadata.tags.push(upperFirst(type));
      }
    }

    /* Map size */
    if (FS.existsSync(`${self}/init.con`)) {
      let mapsize = getFileLineContent({
        file: `${self}/init.con`,
        subString: "game.setActiveCombatArea ",
      });
      if (mapsize && mapsize.split(" ")[3] !== undefined) {
        metadata.map.size = parseInt(mapsize.split(" ")[3]);
      }
    }
    if (FS.existsSync(`${self}/init/terrain.con`)) {
      let mapsize = getFileLineContent({
        file: `${self}/init/terrain.con`,
        subString: "GeometryTemplate.worldSize ",
      });
      if (mapsize) {
        metadata.map.size = parseInt(mapsize);
      }
    }

    /* Map control points */
    for (let x = 0; x < metadata.map.gametypes.length; x++) {
      if (
        FS.existsSync(`${self}/${metadata.map.gametypes[x]}/ControlPoints.con`)
      ) {
        let spawns = FS.readFileSync(
          `${self}/${metadata.map.gametypes[x]}/ControlPoints.con`,
          "utf8",
        );
        const controlPointsRegex =
          /Object.create (?<name>.*)[\s\S]*?Object.absolutePosition (?<position>.*)/gm;
        let controlPoints = [];
        for (const match of spawns.matchAll(controlPointsRegex)) {
          let controlPointsPositions = match.groups.position.split("/");
          controlPointsPositions = {
            x: controlPointsPositions[0],
            y: controlPointsPositions[1],
            z: controlPointsPositions[2],
          };
          controlPoints.push({
            name: startCase(match.groups.name),
            id: match.groups.name,
            position: controlPointsPositions,
          });
        }
        metadata.map.controlpoints = controlPoints;
      }
    }

    /* Merge external metadata */
    if (FS.existsSync(`${dataSource}/maps.json`)) {
      const Metadata = require(`${dataSource}/maps.json`);
      if (debug && Metadata.hasOwnProperty(file))
        log.info({
          context: "Read",
          messages: [`${dataSource}/maps.json`.replace(base, "")],
        });
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("title") &&
        Metadata[file].title.length > 0
      ) {
        metadata.title = Metadata[file].title;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("description") &&
        Metadata[file].description.length > 0
      ) {
        metadata.description = Metadata[file].description;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("labels") &&
        Metadata[file].labels.length > 0
      ) {
        metadata.labels = Metadata[file].labels;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("created") &&
        Metadata[file].created.length > 0
      ) {
        metadata.date = Metadata[file].created;
        metadata.map.created = Metadata[file].created;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("creator") &&
        Metadata[file].creator.length > 0
      ) {
        metadata.author = Metadata[file].creator;
        metadata.map.creator = Metadata[file].creator;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("origin") &&
        Metadata[file].origin.length > 0
      ) {
        metadata.origin = Metadata[file].origin;
      }
    }
    if (debug)
      log.info({
        context: "Built",
        messages: ["Metadata", `${dataSource}/maps.json`.replace(base, "")],
        time: start,
        color: "blueBright",
      });
    return metadata;
  }

  static images({
    source: source,
    target: target,
    dataSource: dataSource,
    debug: debug,
  }) {
    let imageFiles = {};
    let loadImage;
    let bannerImage = `${cleanPrefix(source).replaceAll("_", "-")}.png`;
    let sourceBasename = path.basename(source);
    if (FS.existsSync(`${base}/data/${bannerImage}`)) {
      imageFiles["banner"] = `${base}/data/${bannerImage}`;
    }
    source = `${base}/extract/${source}`;
    if (FS.existsSync(`${source}/menu/init.con`)) {
      loadImage = getFileLineContent({
        file: `${source}/menu/init.con`,
        subString: "game.setLoadPicture ",
      });
      if (loadImage) {
        loadImage = Utilities.cleanImagePath({
          path: loadImage,
          name: sourceBasename,
        });
        if (FS.existsSync(`${source}${loadImage}`)) {
          imageFiles["load"] = `${source}${loadImage}`;
        } else if (FS.existsSync(`${path.dirname(source)}/${loadImage}`)) {
          imageFiles["load"] = `${path.dirname(source)}/${loadImage}`;
        }
        let loadImageDataSource = `${dataSource}/media/load/${path.basename(
          loadImage,
          fileExtension(path.basename(loadImage)),
        )}png`;
        if (FS.existsSync(loadImageDataSource)) {
          imageFiles["load"] = loadImageDataSource;
        }
      }
    }
    if (FS.existsSync(`${source}/menu/thumbnail.dds`)) {
      imageFiles["thumbnail"] = `${source}/menu/thumbnail.dds`;
    }
    if (FS.existsSync(`${source}/textures/ingamemap.dds`)) {
      imageFiles["map"] = `${source}/textures/ingamemap.dds`;
    }
    if (Object.keys(imageFiles).length > 0) {
      if (debug)
        log.info({
          context: "Found",
          messages: [`${Object.keys(imageFiles).length} image(s)`],
        });
      for (const [key, value] of Object.entries(imageFiles)) {
        let output = `${target}/${key}.jpg`;
        if (debug)
          log.info({
            context: "Read",
            messages: [value.replace(base, "")],
          });
        image2jpg({
          input: path.resolve(value),
          output: output,
        });
        if (debug)
          log.info({
            context: "Wrote",
            messages: [`/content${output.replace(dist, "")}`],
            color: "blueBright",
          });
      }
    }
    if (debug)
      log.info({
        context: "Built",
        messages: ["Image(s)"],
        color: "blueBright",
        time: start,
      });
    return;
  }

  static derivative({
    gameBasename: gameBasename,
    modBasename: modBasename,
    sourceBasename: sourceBasename,
    mapBasename: mapBasename,
    debug: debug,
  }) {
    let derivatives = Derivatives;
    const index = derivatives.findIndex(
      (derivative) =>
        derivative === `${gameBasename}/${modBasename}/${sourceBasename}`,
    );
    if (index > -1) {
      derivatives = derivatives.slice(0, index);
      derivatives = derivatives.filter(
        (e) =>
          e !==
          `${gameBasename}/${modBasename}/${sourceBasename}/${cleanPrefix(
            mapBasename,
          )}`,
      );
    }
    let derivation = false;
    for (let i = 0; i < derivatives.length; i++) {
      if (FS.existsSync(`${base}/extract/${derivatives[i]}/${mapBasename}`)) {
        derivation = `/${derivatives[i]}/${cleanPrefix(mapBasename)}`;
        break;
      }
    }
    if (debug && derivation)
      log.info({
        context: ["Derivative"],
        messages: `${gameBasename}/${modBasename}/${sourceBasename}/${cleanPrefix(
          mapBasename,
        )} of ${derivation}`,
        time: start,
      });
    return derivation;
  }
}

module.exports = Battlefield1942;
