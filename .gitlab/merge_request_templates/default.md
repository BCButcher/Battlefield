# Merge Request

## Short Description

## Type of merge

- [ ] Content addition or update
- [ ] Bug fix
- [ ] Feature implementation

## Acceptance criteria

- [ ] Tested locally
- [ ] Content or code has a referenced origin or license, if necessary and possible

## Environment

Version used:
Browser Name and version:
Operating System and version (desktop or mobile):
