const fs = require("fs");
const util = require("util");
const path = require("path");
const lodash = require("lodash");
const { DateTime } = require("luxon");
const sizeOf = require("image-size");
const chalk = require("chalk-cjs");
const metadata = require("./_data/metadata.js");
const Types = require("./SchemaOrg.json");

const getMembers = (members) => {
  let children = [];
  return members
    .map((m) => {
      if (m.children && m.children.length) {
        children = [...children, ...m.children];
      }
      return m;
    })
    .concat(children.length ? getMembers(children) : children);
};

const CreativeWorkType = Types.children.filter(
  (schema) => schema.name == "CreativeWork",
)[0];
const WebPageType = getMembers(
  CreativeWorkType.children.filter((schema) => schema.name == "WebPage"),
);
const WebPageTypes = WebPageType.map((schema) => schema.name);
const ArticleType = getMembers(
  CreativeWorkType.children.filter((schema) => schema.name == "Article"),
);
const ArticleTypes = ArticleType.map((schema) => schema.name);

const logError = (source, message) => {
  console.log(chalk.redBright("ERROR"), chalk.whiteBright(source), message);
};

const {
  createSchemaOrgGraph,
  renderCtxToSchemaOrgJson,
} = require("schema-org-graph-js");
const {
  defineWebSite,
  defineWebPage,
  defineOrganization,
  defineImage,
  definePerson,
  defineSearchAction,
  defineArticle,
} = require("schema-org-graph-js/simple");

let AuthorImage = null;
if (fs.existsSync(metadata.author.image)) {
  AuthorImage = defineImage({
    "@type": "ImageObject",
    url: metadata.author.image,
    contentUrl: metadata.url + metadata.author.image,
    caption: metadata.author.name,
    height: sizeOf(`./public${metadata.author.image}`).height,
    width: sizeOf(`./public${metadata.author.image}`).width,
    inLanguage: metadata.language_bcp47,
  });
}
const Author = definePerson({
  "@type": "Person",
  name: metadata.author.name,
  description: metadata.description,
  url: metadata.url,
  image: AuthorImage,
});

function WebPage({ data: data, type = "WebPage" }) {
  if (lodash.isEmpty(data) || type == "") {
    return false;
  }
  const obj = { "@type": type };
  if (data.hasOwnProperty("url")) {
    obj.url = metadata.url + data.url;
  }
  if (data.hasOwnProperty("title")) {
    obj.name = data.title;
    if (data.hasOwnProperty("subtitle")) {
      obj.name = data.title + ": " + data.subtitle;
    }
  }
  if (data.hasOwnProperty("description")) {
    obj.description = data.description;
  }
  if (Author) {
    obj.author = Author;
  }
  if (metadata.hasOwnProperty("language_bcp47")) {
    obj.inLanguage = metadata.language_bcp47;
  }
  if (data.hasOwnProperty("date")) {
    obj.datePublished = DateTime.fromJSDate(data.date, {
      zone: "utc",
    }).toISO();
  }
  if (data.hasOwnProperty("updated")) {
    obj.dateModified = DateTime.fromJSDate(data.date, {
      zone: "utc",
    }).toISO();
  }
  if (AuthorImage) {
    obj.primaryImageOfPage = AuthorImage;
  }
  return defineWebPage(obj);
}

function Article({ data: data, type = "Article" }) {
  if (lodash.isEmpty(data) || type == "") {
    return false;
  }
  const obj = { "@type": type };
  if (data.hasOwnProperty("title")) {
    obj.headline = data.title;
    if (data.hasOwnProperty("subtitle")) {
      obj.headline = data.title + ": " + data.subtitle;
    }
  }
  if (data.hasOwnProperty("description")) {
    obj.description = data.description;
  }
  if (data.hasOwnProperty("date")) {
    obj.datePublished = DateTime.fromJSDate(data.date, {
      zone: "utc",
    }).toISO();
    obj.copyrightYear = DateTime.fromJSDate(data.date, {
      zone: "utc",
    }).year;
  }
  if (data.hasOwnProperty("updated")) {
    obj.dateModified = DateTime.fromJSDate(data.date, {
      zone: "utc",
    }).toISO();
  }
  if (data.hasOwnProperty("category")) {
    obj.articleSection = data.category;
  }
  if (data.hasOwnProperty("tags")) {
    obj.keywords = data.keywords;
  }
  if (Author) {
    obj.author = Author;
  }
  if (AuthorImage) {
    obj.image = AuthorImage;
  }
  if (metadata.hasOwnProperty("language_bcp47")) {
    obj.inLanguage = metadata.language_bcp47;
  }
  return defineArticle(obj);
}

function render(nodes = []) {
  if (!Array.isArray(nodes)) {
    logError("LinkedData.render()", "nodes-parameter must be passed an array");
    throw new Error("LinkedData.render(nodes: []) must be passed an array");
  }
  let schema;
  let schemaJSON;
  try {
    schema = createSchemaOrgGraph();
    schema.addNode(
      [
        defineWebSite({
          url: metadata.url,
          name: metadata.domain,
          description: metadata.description,
          inLanguage: metadata.language_bcp47,
          publisher: Author,
          potentialAction: defineSearchAction({
            target: "/search/?query={search_term_string}",
          }),
        }),
      ].concat(nodes),
    );
  } catch (error) {
    logError("LinkedData.render()", "could not concatenate passed nodes");
    throw new Error(error);
  }
  try {
    schemaJSON = renderCtxToSchemaOrgJson(schema, {
      host: metadata.url,
      path: "/",
      title: metadata.domain,
      description: metadata.description,
    });
  } catch (error) {
    logError("LinkedData.render()", "could not build schema");
    throw new Error(error);
  }
  try {
    return `<script type="application/ld+json">${JSON.stringify(
      schemaJSON,
      null,
      2,
    )}</script>`;
  } catch (error) {
    logError("LinkedData.render()", "could not stringify JSON");
    throw new Error(error);
  }
}

module.exports = {
  Types: {
    WebPage: WebPageTypes,
    Article: ArticleTypes,
  },
  Author: Author,
  AuthorImage: AuthorImage,
  WebPage: WebPage,
  Article: Article,
  render: render,
};
