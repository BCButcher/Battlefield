module.exports = {
  title: "Battlefield 42&2",
  domain: "BCButcher.GitLab.io/Battlefield/",
  url: "https://bcbutcher.gitlab.io/battlefield",
  language: "en",
  language_bcp47: "en-GB",
  language_icu: "en_GB",
  tags: "battlefield 1942, battlefield 2",
  description:
    "Information about mods and maps made for Battlefield 1942 and 2.",
  footer:
    "[Battlefield 42&2](https://gitlab.com/BCButcher/Battlefield) by [Butcher](https://gitlab.com/BCButcher). All content copyright by their respective, original authors.",
  author: {
    name: "Butcher",
    url: "https://gitlab.com/BCButcher",
  },
  type: "WebPage",
};
