const { registerFont, createCanvas, Image } = require("canvas");
registerFont("./fonts/OpenSans/OpenSans-VariableFont_wdth,wght.ttf", {
  family: "Open Sans",
});

/**
 * Composite image with background, watermark, and text.
 * @param {string} backgroundImage Base64-encoded image
 * @param {string} watermarkImage Base64-encoded image
 * @param {string} title Text for main position
 * @param {string} author Text for bottom left position, if no watermark
 * @param {string} site Text for bottom right position
 * @param {int} width Alternate image width, if not default of 1200
 * @param {int} height Alternate image height, if not default of 630
 * @param {int} fontSize Alternate font size, if not default of 60
 * @returns {Buffer} Canvas as medium-quality JPG for storage
 */
const createImage = ({
  background = "",
  watermark = "",
  title = "",
  author = "",
  site = "",
  width = 1200,
  height = 630,
  fontSize = 60,
}) => {
  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext("2d");
  ctx.imageSmoothingEnabled = true;

  // Draw base image
  const backgroundImage = new Image();
  backgroundImage.onload = () => ctx.drawImage(backgroundImage, 0, 0);
  backgroundImage.onerror = (err) => {
    console.error(err);
  };
  backgroundImage.src = background;

  // Draw gradient overlay
  const gradient = ctx.createLinearGradient(0, 0, 0, 150);
  gradient.addColorStop(0, "rgba(0,0,0,0.15)");
  gradient.addColorStop(1, "rgba(0,0,0,0.35)");
  ctx.fillStyle = gradient;
  ctx.fillRect(0, 0, width, height);

  // Draw the title over multiple lines
  ctx.shadowColor = "#000000";
  ctx.shadowBlur = 3;
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.textAlign = "left";
  ctx.textBaseline = "top";
  ctx.fillStyle = "#ffffff";
  ctx.font = `bold ${fontSize}pt Open Sans`;
  const words = title.split(" ");
  let line = "";
  let fromTop = 70;
  words.forEach((word) => {
    let testLine = line + word + " ";
    if (ctx.measureText(testLine).width > width) {
      ctx.fillText(line.trim(), 60, fromTop);
      line = word + " ";
      fromTop = fromTop + 125;
    } else {
      line = line + word + " ";
    }
  });
  ctx.fillText(line.trim(), 60, fromTop);

  // Draw watermark image or bottom left corner text
  if (watermark !== "") {
    const watermarkImage = new Image();
    watermarkImage.onload = () =>
      ctx.drawImage(watermarkImage, 60, 532, 64, 64);
    watermarkImage.onerror = (err) => {
      console.error(err);
    };
    watermarkImage.src = watermark;
  } else if (author !== "") {
    ctx.font = `${fontSize / 2.5}pt Open Sans`;
    ctx.fillText(author, 60, 540);
  }

  // Draw bottom right corner text
  ctx.font = `${fontSize / 2.5}pt Open Sans`;
  ctx.textAlign = "right";
  ctx.fillText(site, 1140, 540);

  return canvas.toBuffer("image/jpeg", {
    quality: 0.75,
    progressive: true,
    chromaSubsampling: true,
  });
};

module.exports = {
  createImage,
};
