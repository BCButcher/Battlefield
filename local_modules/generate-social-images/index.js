const FS = require("fs"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  chalk = require("chalk"),
  walker = require("klaw-sync"),
  matter = require("gray-matter"),
  readChunk = require("read-chunk"),
  imageType = require("image-type"),
  sizeOf = require("image-size"),
  { getArgs, msToTime } = require("./utilities"),
  { createImage } = require("./create-image");
const args = getArgs();
const start = performance.now();

let base, metadata, background, watermark;
if (args.hasOwnProperty("base") && args.base !== "") {
  base = path.resolve(args.base).replace(/\\/g, "/");
  if (args.debug) console.log(chalk.cyanBright("BASE"), base);
} else {
  return;
}
if (args.hasOwnProperty("metadata") && args.metadata !== "") {
  metadata = require(`${base}${args.metadata}`);
  if (args.debug) console.log(chalk.cyanBright("METADATA"), metadata);
} else {
  return;
}

if (
  args.hasOwnProperty("background") &&
  args.background !== "" &&
  FS.existsSync(`${base}${args.background}`)
) {
  let backgroundImage = `${base}${args.background}`;
  let backgroundImageMimeType = imageType(
    readChunk.sync(backgroundImage, 0, 12),
  );
  let backgroundImageContents = FS.readFileSync(backgroundImage, {
    encoding: "base64",
  });
  background = `data:${backgroundImageMimeType};base64,${backgroundImageContents}`;
  if (args.debug)
    console.log(
      chalk.cyanBright("BACKGROUND"),
      backgroundImageMimeType,
      backgroundImageContents.length,
    );
}
if (
  args.hasOwnProperty("watermark") &&
  args.watermark !== "" &&
  FS.existsSync(`${base}${args.watermark}`)
) {
  let watermarkImage = `${base}${args.watermark}`;
  let watermarkImageMimeType = imageType(readChunk.sync(watermarkImage, 0, 12));
  let watermarkImageContents = FS.readFileSync(watermarkImage, {
    encoding: "base64",
  });
  watermark = `data:${watermarkImageMimeType};base64,${watermarkImageContents}`;
  if (args.debug)
    console.log(
      chalk.cyanBright("WATERMARK"),
      watermarkImageMimeType,
      watermarkImageContents.length,
    );
}

console.log(chalk.magenta("GENERATING SOCIAL IMAGES ..."));

const mainImage = createImage({
  background: background,
  watermark: watermark,
  title: metadata.title,
  author: metadata.title,
  site: metadata.domain,
});
FS.writeFile(`${base}/content/social.jpg`, mainImage, "base64", function (err) {
  if (err) console.error(err);
  if (args.debug)
    console.log(
      chalk.cyanBright("WROTE"),
      `/content/social.jpg in`,
      chalk.whiteBright(msToTime(performance.now() - start)),
    );
});

const pageFolders = walker(`${base}/content`, {
  fs: FS,
  nofile: true,
  depthLimit: -1,
  traverseAll: true,
});
let pages = [];
for (let i = 0; i < pageFolders.length; i++) {
  let pagePath = pageFolders[i].path;
  if (FS.existsSync(`${pagePath}/index.md`)) {
    let metadata = matter(
      FS.readFileSync(`${pagePath}/index.md`, { encoding: "utf8" }),
    ).data;
    const imageFiles = walker(pagePath, {
      fs: FS,
      nodir: true,
      depthLimit: 0,
      filter: (item) => {
        if (
          [".jpg", ".jpeg", ".png"].includes(path.extname(item.path)) &&
          path.basename(item.path) != "social.jpg"
        ) {
          return true;
        }
        return false;
      },
    });
    let images = [];
    for (let n = 0; n < imageFiles.length; n++) {
      const dimensions = sizeOf(imageFiles[n].path);
      if (dimensions.width >= 1200 && dimensions.height >= 630) {
        images.push(imageFiles[n].path);
      }
    }
    pagePath = pagePath.replace(/\\/g, "/").replace(base, "");
    if (metadata.hasOwnProperty("title") && metadata.title !== "") {
      pages.push({ path: pagePath, metadata: metadata, images: images });
    }
  }
}
console.log(
  chalk.magentaBright("PROCESSING"),
  pages.length,
  `Page(s) in ${base}/content`,
);

for (let i = 0; i < pages.length; i++) {
  const start = performance.now();
  let customBackground = null;
  try {
    if (pages[i].hasOwnProperty("images") && pages[i].images.length > 0) {
      let backgroundImage = pages[i].images[0];
      let backgroundImageMimeType = imageType(
        readChunk.sync(pages[i].images[0], 0, 12),
      );
      let backgroundImageContents = FS.readFileSync(backgroundImage, {
        encoding: "base64",
      });
      customBackground = `data:${backgroundImageMimeType};base64,${backgroundImageContents}`;
    }
    const image = createImage({
      background: customBackground ?? background,
      watermark: watermark,
      title: pages[i].metadata.title,
      author: metadata.title,
      site: metadata.domain,
    });
    FS.writeFile(
      `${base}${pages[i].path}/social.jpg`,
      image,
      "base64",
      function (err) {
        if (err) console.error(err);
        if (args.debug)
          console.log(
            chalk.cyanBright("WROTE"),
            `${base}${pages[i].path}/social.jpg in`,
            chalk.whiteBright(msToTime(performance.now() - start)),
          );
      },
    );
  } catch (err) {
    console.error(err);
  }
}

process.on("exit", function () {
  console.log(
    chalk.magentaBright("PROCESSED"),
    pages.length,
    `Page(s) in ${base}/content in`,
    chalk.whiteBright(msToTime(performance.now() - start)),
  );
});
